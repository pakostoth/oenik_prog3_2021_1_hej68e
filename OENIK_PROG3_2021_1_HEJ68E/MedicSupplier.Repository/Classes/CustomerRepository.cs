﻿// -----------------------------------------------------------------------
// <copyright file="CustomerRepository.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MedicSupplier.Repository
{
    using System;
    using System.Linq;
    using MedicSupplier.Data.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Represents the customer repository.
    /// </summary>
    public class CustomerRepository : MyRepository<Customer>, ICustomerRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerRepository"/> class.
        /// </summary>
        /// <param name="ctx">The database context class reference.</param>
        public CustomerRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <inheritdoc/>
        public void ChangeAddress(int id, string address)
        {
            var customer = this.GetOne(id);
            if (customer == null)
            {
                throw new InvalidOperationException("Not found");
            }

            customer.Address = address;
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void ChangeCountry(int id, string country)
        {
            var customer = this.GetOne(id);
            if (customer == null)
            {
                throw new InvalidOperationException("Not found");
            }

            customer.Country = country;
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void ChangeEmail(int id, string email)
        {
            var customer = this.GetOne(id);
            if (customer == null)
            {
                throw new InvalidOperationException("Not found");
            }

            customer.Email = email;
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void ChangeLastOrder(int id, DateTime lastOrder)
        {
            var customer = this.GetOne(id);
            if (customer == null)
            {
                throw new InvalidOperationException("Not found");
            }

            customer.LastOrderDate = lastOrder;
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void ChangeName(int id, string name)
        {
            var customer = this.GetOne(id);
            if (customer == null)
            {
                throw new InvalidOperationException("Not found");
            }

            customer.Name = name;
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void ChangePhone(int id, string phone)
        {
            var customer = this.GetOne(id);
            if (customer == null)
            {
                throw new InvalidOperationException("Not found");
            }

            customer.PhoneNumber = phone;
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public override Customer GetOne(int id)
        {
            return base.GetOne(id);
        }

        /// <inheritdoc/>
        public override void Remove(Customer entity)
        {
            base.Remove(entity);
        }
    }
}
