﻿// -----------------------------------------------------------------------
// <copyright file="OrderRepository.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MedicSupplier.Repository
{
    using System;
    using MedicSupplier.Data.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Represents the order repository.
    /// </summary>
    public class OrderRepository : MyRepository<Order>, IOrderRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OrderRepository"/> class.
        /// </summary>
        /// <param name="ctx">The database context class reference.</param>
        public OrderRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <inheritdoc/>
        public void ChangeDeadline(int id, DateTime deadline)
        {
            var order = this.GetOne(id);
            if (order == null)
            {
                throw new InvalidOperationException("Not found");
            }

            order.Deadline = deadline;
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void ChangeQuantity(int id, int quantity)
        {
            var order = this.GetOne(id);
            if (order == null)
            {
                throw new InvalidOperationException("Not found");
            }

            order.Quantity = quantity;
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public override Order GetOne(int id)
        {
            return base.GetOne(id);
        }
    }
}
