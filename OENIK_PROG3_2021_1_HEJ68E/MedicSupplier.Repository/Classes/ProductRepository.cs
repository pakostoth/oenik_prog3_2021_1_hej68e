﻿// -----------------------------------------------------------------------
// <copyright file="ProductRepository.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MedicSupplier.Repository
{
    using System;
    using MedicSupplier.Data.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Represents the product repository.
    /// </summary>
    public class ProductRepository : MyRepository<Product>, IProductRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProductRepository"/> class.
        /// </summary>
        /// <param name="ctx">The database context class reference.</param>
        public ProductRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <inheritdoc/>
        public void ChangeClassification(int id, string classif)
        {
            var product = this.GetOne(id);
            if (product == null)
            {
                throw new InvalidOperationException("Not found");
            }

            product.Classification = classif;
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void ChangeInstock(int id, int instock)
        {
            var product = this.GetOne(id);
            if (product == null)
            {
                throw new InvalidOperationException("Not found");
            }

            product.InStock = instock;
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void ChangeName(int id, string name)
        {
            var product = this.GetOne(id);
            if (product == null)
            {
                throw new InvalidOperationException("Not found");
            }

            product.Name = name;
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void ChangePrice(int id, double price)
        {
            var product = this.GetOne(id);
            if (product == null)
            {
                throw new InvalidOperationException("Not found");
            }

            product.Price = price;
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void ChangeReorder(int id, int reorder)
        {
            var product = this.GetOne(id);
            if (product == null)
            {
                throw new InvalidOperationException("Not found");
            }

            product.ReorderLevel = reorder;
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public override Product GetOne(int id)
        {
            return base.GetOne(id);
        }
    }
}
