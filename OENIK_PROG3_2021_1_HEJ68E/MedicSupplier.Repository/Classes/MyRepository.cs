﻿// -----------------------------------------------------------------------
// <copyright file="MyRepository.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;

[assembly: CLSCompliant(false)]

namespace MedicSupplier.Repository
{
    using System.Linq;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The main repository, inherited by the others.
    /// </summary>
    /// <typeparam name="T"> The generic type.</typeparam>
    public abstract class MyRepository<T> : IRepository<T>
        where T : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MyRepository{T}"/> class.
        /// </summary>
        /// <param name="ctx"> The database parameter.</param>
        protected MyRepository(DbContext ctx)
        {
            this.Ctx = ctx;
        }

        /// <summary>
        /// Gets or sets the database context used by the repository.
        /// </summary>
        protected virtual DbContext Ctx { get; set; }

        /// <inheritdoc/>
        public virtual T GetOne(int id)
        {
            return this.Ctx.Find<T>(id);
        }

        /// <inheritdoc/>
        public IQueryable<T> GetAll()
        {
            return this.Ctx.Set<T>();
        }

        /// <inheritdoc/>
        public void Insert(T entity)
        {
            this.Ctx.Add(entity);
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public virtual void Remove(T entity)
        {
            this.Ctx.Remove(entity);
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void Remove(int id)
        {
            this.Ctx.Remove(this.GetOne(id));
            this.Ctx.SaveChanges();
        }
    }
}
