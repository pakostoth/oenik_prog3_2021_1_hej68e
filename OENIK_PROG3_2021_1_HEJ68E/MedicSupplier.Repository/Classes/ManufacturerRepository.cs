﻿// -----------------------------------------------------------------------
// <copyright file="ManufacturerRepository.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MedicSupplier.Repository
{
    using System;
    using MedicSupplier.Data.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Represents the manufacturer repository.
    /// </summary>
    public class ManufacturerRepository : MyRepository<Manufacturer>, IManufacturerRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ManufacturerRepository"/> class.
        /// </summary>
        /// <param name="ctx">The database context class reference.</param>
        public ManufacturerRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <inheritdoc/>
        public void ChangeAddress(int id, string address)
        {
            var manufacturer = this.GetOne(id);
            if (manufacturer == null)
            {
                throw new InvalidOperationException("Not found");
            }

            manufacturer.Address = address;
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void ChangeCountry(int id, string country)
        {
            var manufacturer = this.GetOne(id);
            if (manufacturer == null)
            {
                throw new InvalidOperationException("Not found");
            }

            manufacturer.Country = country;
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void ChangeEmail(int id, string email)
        {
            var manufacturer = this.GetOne(id);
            if (manufacturer == null)
            {
                throw new InvalidOperationException("Not found");
            }

            manufacturer.Email = email;
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void ChangeName(int id, string name)
        {
            var manufacturer = this.GetOne(id);
            if (manufacturer == null)
            {
                throw new InvalidOperationException("Not found");
            }

            manufacturer.Name = name;
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void ChangePhone(int id, string phone)
        {
            var manufacturer = this.GetOne(id);
            if (manufacturer == null)
            {
                throw new InvalidOperationException("Not found");
            }

            manufacturer.PhoneNumber = phone;
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void ChangeRevenue(int id, double revenue)
        {
            var manufacturer = this.GetOne(id);
            if (manufacturer == null)
            {
                throw new InvalidOperationException("Not found");
            }

            manufacturer.Revenue = revenue;
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public override Manufacturer GetOne(int id)
        {
            return base.GetOne(id);
        }
    }
}
