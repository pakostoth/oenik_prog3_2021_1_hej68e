﻿// -----------------------------------------------------------------------
// <copyright file="IManufacturerRepository.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MedicSupplier.Repository
{
    using MedicSupplier.Data.Models;

    /// <summary>
    /// Represents the manufacturer repository interface.
    /// </summary>
    public interface IManufacturerRepository : IRepository<Manufacturer>
    {
        /// <summary>
        /// Changes the manufacturer's name.
        /// </summary>
        /// <param name="id">The id of the entity.</param>
        /// <param name="name">The new name of the manufacturer.</param>
        public void ChangeName(int id, string name);

        /// <summary>
        /// Changes the manufacturer's address.
        /// </summary>
        /// <param name="id">The id of the entity.</param>
        /// <param name="address">The new address of the manufacturer.</param>
        public void ChangeAddress(int id, string address);

        /// <summary>
        /// Changes the manufacturer's country.
        /// </summary>
        /// <param name="id">The id of the entity.</param>
        /// <param name="country">The new country of the manufacturer.</param>
        public void ChangeCountry(int id, string country);

        /// <summary>
        /// Changes the manufacturer's phone number.
        /// </summary>
        /// <param name="id">The id of the entity.</param>
        /// <param name="phone">The new phone number of the manufacturer.</param>
        public void ChangePhone(int id, string phone);

        /// <summary>
        /// Changes the manufacturer's revenue.
        /// </summary>
        /// <param name="id">The id of the entity.</param>
        /// <param name="revenue">The new revenue of the manufacturer.</param>
        public void ChangeRevenue(int id, double revenue);

        /// <summary>
        /// Changes the manufacturer's email address.
        /// </summary>
        /// <param name="id">The id of the entity.</param>
        /// <param name="email">The new email of the manufacturer.</param>
        public void ChangeEmail(int id, string email);
    }
}
