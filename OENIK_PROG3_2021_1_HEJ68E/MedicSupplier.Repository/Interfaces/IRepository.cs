﻿// -----------------------------------------------------------------------
// <copyright file="IRepository.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MedicSupplier.Repository
{
    using System.Linq;

    /// <summary>
    /// Represents the main repository interface, inherited by others.
    /// </summary>
    /// <typeparam name="T"> The generic type.</typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Inserts an entity to the database.
        /// </summary>
        /// <param name="entity">The entity to be inserted.</param>
        public void Insert(T entity);

        /// <summary>
        /// Return a requested entity by id.
        /// </summary>
        /// <param name="id">The id of the requested entity.</param>
        /// <returns>The class of the entity.</returns>
        public T GetOne(int id);

        /// <summary>
        /// Return all of the requested entities.
        /// </summary>
        /// <returns>The list of entities.</returns>
        public IQueryable<T> GetAll();

        /// <summary>
        /// Deletes an entity. Needs the entity class to work.
        /// </summary>
        /// <param name="entity">The entity to be removed.</param>
        public void Remove(T entity);

        /// <summary>
        /// Deletes an entity by id.
        /// </summary>
        /// <param name="id">The id of the entity to be removed.</param>
        public void Remove(int id);
    }
}
