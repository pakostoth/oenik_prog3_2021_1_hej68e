﻿// -----------------------------------------------------------------------
// <copyright file="IOrderRepository.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MedicSupplier.Repository
{
    using System;
    using MedicSupplier.Data.Models;

    /// <summary>
    /// Represents the order repository interface.
    /// </summary>
    public interface IOrderRepository : IRepository<Order>
    {
        /// <summary>
        /// Changes the ordered quantity.
        /// </summary>
        /// <param name="id">The id of the entity.</param>
        /// <param name="quantity">The new quantity.</param>
        public void ChangeQuantity(int id, int quantity);

        /// <summary>
        /// Changes the deadline of the order.
        /// </summary>
        /// <param name="id">The id of the entity.</param>
        /// <param name="deadline">The new deadline.</param>
        public void ChangeDeadline(int id, DateTime deadline);
    }
}
