﻿// -----------------------------------------------------------------------
// <copyright file="ICustomerRepository.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MedicSupplier.Repository
{
    using System;
    using MedicSupplier.Data.Models;

    /// <summary>
    /// Represents the customer repository interface.
    /// </summary>
    public interface ICustomerRepository : IRepository<Customer>
    {
        /// <summary>
        /// Changes the customer's name.
        /// </summary>
        /// <param name="id">The id of the entity.</param>
        /// <param name="name">The new name of the customer.</param>
        public void ChangeName(int id, string name);

        /// <summary>
        /// Changes the customer's address.
        /// </summary>
        /// <param name="id">The id of the entity.</param>
        /// <param name="address">The new address of the customer.</param>
        public void ChangeAddress(int id, string address);

        /// <summary>
        /// Changes the customer's country.
        /// </summary>
        /// <param name="id">The id of the entity.</param>
        /// <param name="country">The new country of the customer.</param>
        public void ChangeCountry(int id, string country);

        /// <summary>
        /// Changes the customer's phone number.
        /// </summary>
        /// <param name="id">The id of the entity.</param>
        /// <param name="phone">The new phone number of the customer.</param>
        public void ChangePhone(int id, string phone);

        /// <summary>
        /// Changes the customer's last order date.
        /// </summary>
        /// <param name="id">The id of the entity.</param>
        /// <param name="lastOrder">The new last order date of the customer.</param>
        public void ChangeLastOrder(int id, DateTime lastOrder);

        /// <summary>
        /// Changes the customer's email address.
        /// </summary>
        /// <param name="id">The id of the entity.</param>
        /// <param name="email">The new email of the customer.</param>
        public void ChangeEmail(int id, string email);
    }
}
