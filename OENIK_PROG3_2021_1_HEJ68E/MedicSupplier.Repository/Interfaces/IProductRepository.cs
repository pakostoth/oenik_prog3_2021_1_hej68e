﻿// -----------------------------------------------------------------------
// <copyright file="IProductRepository.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MedicSupplier.Repository
{
    using MedicSupplier.Data.Models;

    /// <summary>
    /// Represents the product repository interface.
    /// </summary>
    public interface IProductRepository : IRepository<Product>
    {
        /// <summary>
        /// Changes the product's name.
        /// </summary>
        /// <param name="id">The id of the entity.</param>
        /// <param name="name">The new name of the product.</param>
        public void ChangeName(int id, string name);

        /// <summary>
        /// Changes the price of the product.
        /// </summary>
        /// <param name="id">The id of the entity.</param>
        /// <param name="price">The new price of the product.</param>
        public void ChangePrice(int id, double price);

        /// <summary>
        /// Changes the stock quantity of the product.
        /// </summary>
        /// <param name="id">The id of the entity.</param>
        /// <param name="instock">The new quantity in stock.</param>
        public void ChangeInstock(int id, int instock);

        /// <summary>
        /// Changes the reorder level of the product.
        /// </summary>
        /// <param name="id">The id of the entity.</param>
        /// <param name="reorder">The new level of reorder.</param>
        public void ChangeReorder(int id, int reorder);

        /// <summary>
        /// Changes the classification of the product.
        /// </summary>
        /// <param name="id">The id of the entity.</param>
        /// <param name="classif">The new classification.</param>
        public void ChangeClassification(int id, string classif);
    }
}
