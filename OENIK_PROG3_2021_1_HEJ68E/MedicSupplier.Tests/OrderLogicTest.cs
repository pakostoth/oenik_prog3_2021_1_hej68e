﻿// -----------------------------------------------------------------------
// <copyright file="OrderLogicTest.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;

[assembly: CLSCompliant(false)]

namespace MedicSupplier.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using MedicSupplier.Data.Models;
    using MedicSupplier.Logic;
    using MedicSupplier.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Test class for testing the order logic methods.
    /// </summary>
    [TestFixture]
    public class OrderLogicTest
    {
        private readonly Mock<IProductRepository> mockedProductRepo = new Mock<IProductRepository>(MockBehavior.Loose);

        private readonly Mock<ICustomerRepository> mockedCustomerRepo = new Mock<ICustomerRepository>(MockBehavior.Loose);

        private readonly Mock<IOrderRepository> mockedOrderRepo = new Mock<IOrderRepository>(MockBehavior.Loose);

        private List<Product> Products { get; set; }

        private List<Customer> Customers { get; set; }

        private List<Order> Orders { get; set; }

        private OrderLogic OLogic { get; set; }

        /// <summary>
        /// Setup for the testing the order logic.
        /// </summary>
        [OneTimeSetUp]
        public void Setup()
        {
            this.OLogic = new OrderLogic(
                this.mockedCustomerRepo.Object,
                this.mockedProductRepo.Object,
                this.mockedOrderRepo.Object);
            this.Products = new List<Product>()
            {
                new Product() { ProductID = 1, Name = "TestProduct1", InStock = 300, Classification = "1", ManufacturerID = 1, Price = 10, ReorderLevel = 100 },
                new Product() { ProductID = 2, Name = "TestProduct2", InStock = 200, Classification = "2", ManufacturerID = 1, Price = 10, ReorderLevel = 100 },
                new Product() { ProductID = 3, Name = "TestProduct3", InStock = 100, Classification = "2", ManufacturerID = 1, Price = 10, ReorderLevel = 100 },
                new Product() { ProductID = 4, Name = "TestProduct4", InStock = 500, Classification = "1", ManufacturerID = 1, Price = 10, ReorderLevel = 100 },
            };

            this.Customers = new List<Customer>()
            {
                new Customer() { CustomerID = 1, Name = "TestCustomer1", LastOrderDate = DateTime.MinValue },
                new Customer() { CustomerID = 2, Name = "TestCustomer2", LastOrderDate = DateTime.MinValue },
                new Customer() { CustomerID = 3, Name = "TestCustomer3", LastOrderDate = DateTime.MinValue },
                new Customer() { CustomerID = 4, Name = "TestCustomer4", LastOrderDate = DateTime.MinValue },
            };

            this.Orders = new List<Order>()
            {
                new Order() { OrderID = 1, CustomerID = 1, ProductID = 1 },
                new Order() { OrderID = 2, CustomerID = 3, ProductID = 2 },
                new Order() { OrderID = 3, CustomerID = 2, ProductID = 3 },
                new Order() { OrderID = 4, CustomerID = 1, ProductID = 4 },
            };
        }

        /// <summary>
        /// Tests the AddOrder method.
        /// </summary>
        [Test]
        public void TestAddOrder()
        {
            // Arrange
            int id = 2;
            this.mockedOrderRepo.Setup(repo => repo.GetOne(It.Is<int>(id => id > 0 && id < this.Orders.Count))).Returns(this.Orders[id]);
            this.mockedProductRepo.Setup(repo => repo.GetOne(It.Is<int>(id => id > 0 && id < this.Products.Count))).Returns(this.Products[id]);

            // Act
            this.OLogic.AddOrder(2, 3, 100, DateTime.Now);

            // Verify
            this.mockedOrderRepo.Verify(repo => repo.GetOne(42), Times.Exactly(0));
        }

        /// <summary>
        /// Tests the GetOneProduct method.
        /// </summary>
        /// <param name="id">The id of the requested product.</param>
        [Test]
        [TestCase(1)]
        [TestCase(3)]
        public void TestGetOneProduct(int id)
        {
            // Arrange
            Product expectedProduct = this.Products[id];
            this.mockedProductRepo.Setup(repo => repo.GetOne(It.Is<int>(id => id > 0 && id < this.Products.Count))).Returns(this.Products[id]);

            // Act
            var result = this.OLogic.GetOneProduct(id);

            // Assert
            Assert.That(result.ProductID, Is.EqualTo(id + 1));

            // Verify
            this.mockedProductRepo.Verify(repo => repo.GetOne(id), Times.Once);
        }

        /// <summary>
        /// Tests the CustomersWithOrder nonCrud method.
        /// </summary>
        [Test]
        public void TestCustomersWithOrder()
        {
            // Arrange
            this.mockedCustomerRepo.Setup(repo => repo.GetAll()).Returns(this.Customers.AsQueryable());
            this.mockedOrderRepo.Setup(repo => repo.GetAll()).Returns(this.Orders.AsQueryable());
            List<Customer> expectedResult = new List<Customer> { this.Customers[0], this.Customers[1], this.Customers[2] };

            // Act
            var result = this.OLogic.CustomersWithOrder();

            // Assert
            Assert.That(result.Count, Is.EqualTo(expectedResult.Count));

            // Verify
            this.mockedCustomerRepo.Verify(repo => repo.GetAll(), Times.Once);
            this.mockedOrderRepo.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Tests the ProductsAndOrdersbyClassification nonCrud method.
        /// </summary>
        [Test]
        public void TestProductsAndOrdersbyClassification()
        {
            // Arrange
            this.mockedOrderRepo.Setup(repo => repo.GetAll()).Returns(this.Orders.AsQueryable());
            this.mockedProductRepo.Setup(repo => repo.GetAll()).Returns(this.Products.AsQueryable());
            var expectedResult = new List<ProductbyClassificationResult>()
            {
                new ProductbyClassificationResult("1", 1, 1),
                new ProductbyClassificationResult("1", 4, 1),
            };

            // Act
            var result = this.OLogic.ProductsAndOrdersbyClassification("1");

            // Assert
            Assert.That(result, Is.EquivalentTo(expectedResult));

            // Verify
            this.mockedOrderRepo.Verify(repo => repo.GetAll(), Times.AtLeastOnce);
            this.mockedProductRepo.Verify(repo => repo.GetAll(), Times.Once);
            this.mockedCustomerRepo.Verify(repo => repo.GetOne(It.IsAny<int>()), Times.Never);

            // Two
        }
    }
}
