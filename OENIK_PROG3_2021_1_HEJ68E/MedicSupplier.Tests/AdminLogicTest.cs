﻿// -----------------------------------------------------------------------
// <copyright file="AdminLogicTest.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MedicSupplier.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using MedicSupplier.Data.Models;
    using MedicSupplier.Logic;
    using MedicSupplier.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Test class for testing the admin logic methods.
    /// </summary>
    public class AdminLogicTest
    {
        private readonly Mock<IProductRepository> mockedProductRepo = new Mock<IProductRepository>(MockBehavior.Loose);

        private readonly Mock<ICustomerRepository> mockedCustomerRepo = new Mock<ICustomerRepository>(MockBehavior.Loose);

        private readonly Mock<IManufacturerRepository> mockedManufacturerRepo = new Mock<IManufacturerRepository>(MockBehavior.Loose);

        private List<Product> Products { get; set; }

        private List<Customer> Customers { get; set; }

        private List<Manufacturer> Manufacturers { get; set; }

        private AdminLogic ALogic { get; set; }

        /// <summary>
        /// Setup for the testing the admin logic.
        /// </summary>
        [OneTimeSetUp]
        public void Setup()
        {
            this.ALogic = new AdminLogic(
                this.mockedCustomerRepo.Object,
                this.mockedProductRepo.Object,
                this.mockedManufacturerRepo.Object);

            this.Products = new List<Product>()
            {
                new Product() { ProductID = 1, Name = "TestProduct1", ManufacturerID = 1 },
                new Product() { ProductID = 2, Name = "TestProduct2", ManufacturerID = 1 },
                new Product() { ProductID = 3, Name = "TestProduct3", ManufacturerID = 2 },
                new Product() { ProductID = 4, Name = "TestProduct4", ManufacturerID = 2 },
                new Product() { ProductID = 5, Name = "TestProduct4", ManufacturerID = 1 },
            };

            this.Customers = new List<Customer>()
            {
                new Customer() { CustomerID = 1, Name = "TestCustomer1" },
                new Customer() { CustomerID = 2, Name = "TestCustomer2" },
                new Customer() { CustomerID = 3, Name = "TestCustomer3" },
            };

            this.Manufacturers = new List<Manufacturer>()
            {
                new Manufacturer() { ManufacturerID = 1, Name = "TestManuf1", Country = "Belgium" },
                new Manufacturer() { ManufacturerID = 1, Name = "TestManuf1", Country = "Németország" },
            };
        }

        /// <summary>
        /// Tests the GetAllCustomer method, that lists all the customers.
        /// </summary>
        [Test]
        public void TestGetAllCustomer()
        {
            // Arrange
            this.mockedCustomerRepo.Setup(repo => repo.GetAll()).Returns(this.Customers.AsQueryable());

            // Act
            var result = this.ALogic.GetAllCustomer();

            // Assert
            Assert.That(result.Count, Is.EqualTo(this.Customers.Count));

            // Verify
            this.mockedCustomerRepo.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Tests the DeleteCustomer method.
        /// </summary>
        /// <param name="id">The id of the the customer to be removed.</param>
        [Test]
        [TestCase(1)]
        [TestCase(2)]
        public void TestDeleteCustomer(int id)
        {
            // Arrange
            this.mockedCustomerRepo.Setup(repo => repo.Remove(It.Is<int>(id => id > 0 && id < this.Customers.Count)));
            this.mockedCustomerRepo.Setup(repo => repo.Remove(It.IsAny<Customer>()));
            this.mockedCustomerRepo.Setup(repo => repo.GetOne(It.Is<int>(id => id > 0 && id < this.Customers.Count))).Returns(this.Customers[id - 1]);

            // Act
            this.ALogic.DeleteCustomer(id);

            // Verify
            this.mockedCustomerRepo.Verify(repo => repo.Remove(id), Times.Never);
        }

        /// <summary>
        /// Tests the UpdateManufacturerName method.
        /// </summary>
        /// <param name="id">The id of the manufacturer.</param>
        /// <param name="name">The new name of the manufacturer.</param>
        [Test]
        [TestCase(1, "NewName")]
        [TestCase(2, "TestName1")]
        public void TestUpdateManufacturerName(int id, string name)
        {
            // Arrange
            this.mockedManufacturerRepo.Setup(repo => repo.ChangeName(id, name));

            // Act
            this.ALogic.UpdateManufacturerName(id, name);

            // Verify
            this.mockedManufacturerRepo.Verify(repo => repo.ChangeName(id, name), Times.Once);
            this.mockedManufacturerRepo.Verify(repo => repo.ChangeName(id, "Józsi"), Times.Never);
        }

        /// <summary>
        /// Tests the UpdateProductName method.
        /// </summary>
        /// <param name="id">The id of the product.</param>
        /// <param name="name">The new name of the product.</param>
        [Test]
        [TestCase(2, "NewName")]
        public void TestUpdateProductName(int id, string name)
        {
            // Arrange
            this.mockedProductRepo.Setup(repo => repo.ChangeName(id, name));

            // Act
            this.ALogic.UpdateProductName(id, name);

            // Verify
            this.mockedProductRepo.Verify(repo => repo.ChangeName(id, name), Times.Once);
            this.mockedProductRepo.Verify(repo => repo.GetOne(id), Times.Never);
        }

        /// <summary>
        /// Tests the ListProductsByCountry nonCrud method.
        /// </summary>
        /// <param name="country"> The name of the country.</param>
        [Test]
        [TestCase("Belgium")]
        public void TestListProductsByCountry(string country)
        {
            // Arrange
            this.mockedProductRepo.Setup(repo => repo.GetAll()).Returns(this.Products.AsQueryable());
            this.mockedManufacturerRepo.Setup(repo => repo.GetAll()).Returns(this.Manufacturers.AsQueryable());
            var expectedResult = new List<Product>()
            {
                this.Products[0],
                this.Products[1],
                this.Products[4],
            };

            // Act
            var result = this.ALogic.ListProductsByCountry(country);

            // Assert
            Assert.That(result.Count, Is.EqualTo(expectedResult.Count));
            Assert.That(result, Is.EquivalentTo(expectedResult));

            // Verify
            this.mockedProductRepo.Verify(repo => repo.GetAll(), Times.Once);
            this.mockedManufacturerRepo.Verify(repo => repo.GetOne(3), Times.Never);
        }
    }
}
