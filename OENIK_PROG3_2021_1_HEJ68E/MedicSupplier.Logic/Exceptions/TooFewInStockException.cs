﻿// -----------------------------------------------------------------------
// <copyright file="TooFewInStockException.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MedicSupplier.Logic
{
    using System;
    using System.Globalization;

    /// <summary>
    /// Thrown when there are less quantity in stock then the order require.
    /// Thrown when the order should be added.
    /// </summary>
    public class TooFewInStockException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TooFewInStockException"/> class.
        /// </summary>
        /// <param name="message">The message thrown to the base exception.</param>
        public TooFewInStockException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TooFewInStockException"/> class.
        /// </summary>
        public TooFewInStockException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TooFewInStockException"/> class.
        /// </summary>
        /// <param name="quantity">The instock quantity of the product.</param>
        public TooFewInStockException(int quantity)
        {
            string message = quantity.ToString(new CultureInfo("hu-HU"));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TooFewInStockException"/> class.
        /// </summary>
        /// <param name="message">The message thrown to the base exception.</param>
        /// <param name="innerException">The inner exception.</param>
        public TooFewInStockException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
