﻿// -----------------------------------------------------------------------
// <copyright file="OrderLogic.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;

[assembly: CLSCompliant(false)]

namespace MedicSupplier.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using MedicSupplier.Data.Models;
    using MedicSupplier.Repository;

    /// <summary>
    /// Represents an orderlogic object.
    /// </summary>
    public class OrderLogic : IOrderLogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OrderLogic"/> class.
        /// </summary>
        /// <param name="customerRepo">A customer repository object.</param>
        /// <param name="productRepo">A product repository object.</param>
        /// <param name="orderRepo">An order repository object.</param>
        public OrderLogic(ICustomerRepository customerRepo, IProductRepository productRepo, IOrderRepository orderRepo)
        {
            this.CustomerRepo = customerRepo;
            this.ProductRepo = productRepo;
            this.OrderRepo = orderRepo;
        }

        /// <summary>
        /// Gets or sets a customer repository object in the order logic.
        /// </summary>
        public ICustomerRepository CustomerRepo { get; set; }

        /// <summary>
        /// Gets or sets a product repository object in the order logic.
        /// </summary>
        public IProductRepository ProductRepo { get; set; }

        /// <summary>
        /// Gets or sets a order repository object in the order logic.
        /// </summary>
        public IOrderRepository OrderRepo { get; set; }

        /// <inheritdoc/>
        public void AddOrder(int productID, int customerID, int quantity, DateTime deadline)
        {
            int instock = this.ProductRepo.GetOne(productID).InStock;
            if (instock < quantity)
            {
                throw new TooFewInStockException(instock);
            }

            Order newOrder = new Order(productID, customerID, quantity, deadline);
            this.OrderRepo.Insert(newOrder);

            // Adding the current date to the customer's last order date
            this.CustomerRepo.ChangeLastOrder(customerID, new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day));
        }

        /// <inheritdoc/>
        public void DeleteOrder(int id)
        {
            // Adding back the ordered quantity
            this.ProductRepo.GetOne(this.OrderRepo.GetOne(id).ProductID).InStock += this.OrderRepo.GetOne(id).Quantity;
            this.OrderRepo.Remove(this.OrderRepo.GetOne(id));
        }

        /// <inheritdoc/>
        public IList<Order> GetAllOrder()
        {
            return this.OrderRepo.GetAll().ToList();
        }

        /// <inheritdoc/>
        public IList<Product> GetAllProduct()
        {
            return this.ProductRepo.GetAll().ToList();
        }

        /// <inheritdoc/>
        public Order GetOneOrder(int id)
        {
            return this.OrderRepo.GetOne(id);
        }

        /// <inheritdoc/>
        public Product GetOneProduct(int id)
        {
            return this.ProductRepo.GetOne(id);
        }

        /// <inheritdoc/>
        public void UpdateLastOrderDate(int id, DateTime lastOrder)
        {
            this.CustomerRepo.ChangeLastOrder(id, lastOrder);
        }

        /// <inheritdoc/>
        public void UpdateOrderDeadline(int id, DateTime deadline)
        {
            this.OrderRepo.ChangeDeadline(id, deadline);
        }

        /// <inheritdoc/>
        public void UpdateOrderQuantity(int id, int quantity)
        {
            this.OrderRepo.ChangeQuantity(id, quantity);
        }

        /// <inheritdoc/>
        public void UpdateProductClassification(int id, string classif)
        {
            this.ProductRepo.ChangeClassification(id, classif);
        }

        /// <inheritdoc/>
        public void UpdateProductOrderLevel(int id, int orderlevel)
        {
            this.ProductRepo.ChangeReorder(id, orderlevel);
        }

        /// <inheritdoc/>
        public void UpdateProductPrice(int id, double price)
        {
            this.ProductRepo.ChangePrice(id, price);
        }

        /// <inheritdoc/>
        public void UpdateProductQuantity(int id, int quantity)
        {
            this.ProductRepo.ChangeInstock(id, quantity);
        }

        /// <inheritdoc/>
        public ICollection<Customer> CustomersWithOrder()
        {
            var customers = from customer in this.CustomerRepo.GetAll()
                            join order in this.OrderRepo.GetAll()
                            on customer.CustomerID equals order.CustomerID
                            select customer;
            return customers.Distinct().ToList();
        }

        /// <inheritdoc/>
        public async Task<ICollection<Customer>> CustomersWithOrderAsync()
        {
            return await Task.Run(() => this.CustomersWithOrder()).ConfigureAwait(true);
        }

        /// <inheritdoc/>
        public ICollection<ProductbyClassificationResult> ProductsAndOrdersbyClassification(string classif)
        {
            var results = from product in this.ProductRepo.GetAll()
                          join order in this.OrderRepo.GetAll()
                          on product.ProductID equals order.ProductID
                          where product.Classification == classif
                          group order by order.ProductID into grp
                          select new ProductbyClassificationResult(
                              classif,
                              grp.Key,
                              grp.Count());
            return results.ToList();
        }

        /// <inheritdoc/>
        public async Task<ICollection<ProductbyClassificationResult>> ProductsAndOrdersbyClassificationAsync(string classif)
        {
            return await Task.Run(() => this.ProductsAndOrdersbyClassification(classif)).ConfigureAwait(true);
        }

        /// <inheritdoc/>
        public IQueryable<string> ListAllClassification()
        {
            var classifs = from product in this.ProductRepo.GetAll()
                           group product by product.Classification into grp
                           select grp.Key;
            return classifs;
        }
    }
}
