﻿// -----------------------------------------------------------------------
// <copyright file="IOrderLogic.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MedicSupplier.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using MedicSupplier.Data.Models;

    /// <summary>
    /// The logic interface for ordering methods.
    /// </summary>
    public interface IOrderLogic
    {
        /// <summary>
        /// Add a new order to the database.
        /// </summary>
        /// <param name="productID">The id of the ordered product.</param>
        /// <param name="customerID">The id of the ordering customer.</param>
        /// <param name="quantity">The quantity of the product.</param>
        /// <param name="deadline">The deadline of the order.</param>
        public void AddOrder(int productID, int customerID, int quantity, DateTime deadline);

        /// <summary>
        /// Lists one specific order from the database.
        /// </summary>
        /// <param name="id">The id of the order to be listed.</param>
        /// <returns>The order object to list.</returns>
        public Order GetOneOrder(int id);

        /// <summary>
        /// Lists all orders from the database.
        /// </summary>
        /// <returns>The orders' list.</returns>
        public IList<Order> GetAllOrder();

        /// <summary>
        /// Deletes one specific order from the database.
        /// </summary>
        /// <param name="id">The id of the order.</param>
        public void DeleteOrder(int id);

        /// <summary>
        /// Updates one specific order's quantity field.
        /// </summary>
        /// <param name="id">The id of the order.</param>
        /// <param name="quantity">The new quantity.</param>
        public void UpdateOrderQuantity(int id, int quantity);

        /// <summary>
        /// Updates one specific order's deadline field.
        /// </summary>
        /// <param name="id">The id of the order.</param>
        /// <param name="deadline">The new deadline.</param>
        public void UpdateOrderDeadline(int id, DateTime deadline);

        /// <summary>
        /// Updates one specific customer's last order date field.
        /// </summary>
        /// <param name="id">The id of the customer.</param>
        /// <param name="lastOrder">The new last order date.</param>
        public void UpdateLastOrderDate(int id, DateTime lastOrder);

        /// <summary>
        /// List one specific product from the database.
        /// </summary>
        /// <param name="id">The id of the product.</param>
        /// <returns>The specific product object.</returns>
        public Product GetOneProduct(int id);

        /// <summary>
        /// List all products from the database.
        /// </summary>
        /// <returns>The products' list.</returns>
        public IList<Product> GetAllProduct();

        /// <summary>
        /// Updates one specific product's price field.
        /// </summary>
        /// <param name="id">The id of the product.</param>
        /// <param name="price">The new price.</param>
        public void UpdateProductPrice(int id, double price);

        /// <summary>
        /// Updates one specific product's price field.
        /// </summary>
        /// <param name="id">The id of the product.</param>
        /// <param name="orderlevel">The new reorder level.</param>
        public void UpdateProductOrderLevel(int id, int orderlevel);

        /// <summary>
        /// Updates one specific product's price field.
        /// </summary>
        /// <param name="id">The id of the product.</param>
        /// <param name="quantity">The new quantity.</param>
        public void UpdateProductQuantity(int id, int quantity);

        /// <summary>
        /// Updates one specific product's price field.
        /// </summary>
        /// <param name="id">The id of the product.</param>
        /// <param name="classif">The new calssification.</param>
        public void UpdateProductClassification(int id, string classif);

        /// <summary>
        /// Lists the customers who has atleast one order.
        /// </summary>
        /// <returns>The customers' list.</returns>
        public ICollection<Customer> CustomersWithOrder();

        /// <summary>
        /// Lists the customers who has atleast one order. Async version.
        /// </summary>
        /// <returns>The task of the customers' list.</returns>
        public Task<ICollection<Customer>> CustomersWithOrderAsync();

        /// <summary>
        /// Returns the orders by products by a specific classification.
        /// </summary>
        /// <param name="classif">The classification of the products.</param>
        /// <returns>The results' collection.</returns>
        public ICollection<ProductbyClassificationResult> ProductsAndOrdersbyClassification(string classif);

        /// <summary>
        /// Returns the orders by products by a specific classification. Async version.
        /// </summary>
        /// <param name="classif">The classification of the products.</param>
        /// <returns>The task of the results' collection.</returns>
        public Task<ICollection<ProductbyClassificationResult>> ProductsAndOrdersbyClassificationAsync(string classif);

        /// <summary>
        /// List the classifications.
        /// </summary>
        /// <returns>Return the classifications as a collection.</returns>
        public IQueryable<string> ListAllClassification();
    }
}
