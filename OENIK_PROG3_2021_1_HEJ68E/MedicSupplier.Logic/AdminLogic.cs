﻿// -----------------------------------------------------------------------
// <copyright file="AdminLogic.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MedicSupplier.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using MedicSupplier.Data.Models;
    using MedicSupplier.Repository;

    /// <summary>
    /// Represents an adminlogic object.
    /// </summary>
    public class AdminLogic : IAdminLogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AdminLogic"/> class.
        /// </summary>
        /// <param name="customerRepo">A customer repository object.</param>
        /// <param name="productRepo">A product repository object.</param>
        /// <param name="manufacturerRepo">A manufacturer repository object.</param>
        public AdminLogic(ICustomerRepository customerRepo, IProductRepository productRepo, IManufacturerRepository manufacturerRepo)
        {
            this.CustomerRepo = customerRepo;
            this.ProductRepo = productRepo;
            this.ManufacturerRepo = manufacturerRepo;
        }

        /// <summary>
        /// Gets or sets a customer repository object in the admin logic.
        /// </summary>
        public ICustomerRepository CustomerRepo { get; set; }

        /// <summary>
        /// Gets or sets a product repository object in the admin logic.
        /// </summary>
        public IProductRepository ProductRepo { get; set; }

        /// <summary>
        /// Gets or sets a manufacturer repository object in the admin logic.
        /// </summary>
        public IManufacturerRepository ManufacturerRepo { get; set; }

        /// <inheritdoc/>
        public void AddCustomer(string name, string address, string country, string phone, string email)
        {
            Customer newCustomer = new Customer(name, address, country, phone, DateTime.MinValue, email);
            this.CustomerRepo.Insert(newCustomer);
        }

        /// <inheritdoc/>
        public void AddManufacturer(string name, string address, string country, string phone, double revenue, string email)
        {
            Manufacturer newManufacturer = new Manufacturer(name, address, country, phone, revenue, email);
            this.ManufacturerRepo.Insert(newManufacturer);
        }

        /// <inheritdoc/>
        public void AddProduct(int manufacturerID, string name, double price, int inStock, int reorder, string classif)
        {
            Product newProduct = new Product(manufacturerID, name, price, inStock, reorder, classif);
            this.ProductRepo.Insert(newProduct);
        }

        /// <inheritdoc/>
        public void DeleteCustomer(int id)
        {
            this.CustomerRepo.Remove(this.CustomerRepo.GetOne(id));
        }

        /// <inheritdoc/>
        public void DeleteManufacturer(int id)
        {
            this.ManufacturerRepo.Remove(this.ManufacturerRepo.GetOne(id));
        }

        /// <inheritdoc/>
        public void DeleteProduct(int id)
        {
            this.ProductRepo.Remove(this.ProductRepo.GetOne(id));
        }

        /// <inheritdoc/>
        public IList<Customer> GetAllCustomer()
        {
            return this.CustomerRepo.GetAll().ToList();
        }

        /// <inheritdoc/>
        public IList<Manufacturer> GetAllManufacturer()
        {
            return this.ManufacturerRepo.GetAll().ToList();
        }

        /// <inheritdoc/>
        public Customer GetOneCustomer(int id)
        {
            return this.CustomerRepo.GetOne(id);
        }

        /// <inheritdoc/>
        public Manufacturer GetOneManufacturer(int id)
        {
            return this.ManufacturerRepo.GetOne(id);
        }

        /// <inheritdoc/>
        public void UpdateCustomerAddress(int id, string address)
        {
            this.CustomerRepo.ChangeAddress(id, address);
        }

        /// <inheritdoc/>
        public void UpdateCustomerCountry(int id, string country)
        {
            this.CustomerRepo.ChangeCountry(id, country);
        }

        /// <inheritdoc/>
        public void UpdateCustomerEmail(int id, string email)
        {
            this.CustomerRepo.ChangeEmail(id, email);
        }

        /// <inheritdoc/>
        public void UpdateCustomerName(int id, string name)
        {
            this.CustomerRepo.ChangeName(id, name);
        }

        /// <inheritdoc/>
        public void UpdateCustomerPhone(int id, string phoneNumber)
        {
            this.CustomerRepo.ChangePhone(id, phoneNumber);
        }

        /// <inheritdoc/>
        public void UpdateManufacturerAddress(int id, string address)
        {
            this.ManufacturerRepo.ChangeAddress(id, address);
        }

        /// <inheritdoc/>
        public void UpdateManufacturerCountry(int id, string country)
        {
            this.ManufacturerRepo.ChangeCountry(id, country);
        }

        /// <inheritdoc/>
        public void UpdateManufacturerEmail(int id, string email)
        {
            this.ManufacturerRepo.ChangeEmail(id, email);
        }

        /// <inheritdoc/>
        public void UpdateManufacturerName(int id, string name)
        {
            this.ManufacturerRepo.ChangeName(id, name);
        }

        /// <inheritdoc/>
        public void UpdateManufacturerPhone(int id, string phoneNumber)
        {
            this.ManufacturerRepo.ChangePhone(id, phoneNumber);
        }

        /// <inheritdoc/>
        public void UpdateManufacturerRevenue(int id, double revenue)
        {
            this.ManufacturerRepo.ChangeRevenue(id, revenue);
        }

        /// <inheritdoc/>
        public void UpdateProductName(int id, string name)
        {
            this.ProductRepo.ChangeName(id, name);
        }

        /// <inheritdoc/>
        public ICollection<Product> ListProductsByCountry(string country)
        {
            var products = from product in this.ProductRepo.GetAll()
                           join manuf in this.ManufacturerRepo.GetAll()
                           on product.ManufacturerID equals manuf.ManufacturerID
                           where manuf.Country == country
                           select product;
            return products.ToList();
        }

        /// <inheritdoc/>
        public async Task<ICollection<Product>> ListProductsByCountryAsync(string country)
        {
            return await Task.Run(() => this.ListProductsByCountry(country)).ConfigureAwait(true);
        }

        /// <inheritdoc/>
        public IQueryable<string> ListAllCountry()
        {
            var countries = from manufacturer in this.ManufacturerRepo.GetAll()
                            group manufacturer by manufacturer.Country into grp
                            select grp.Key;
            return countries;
        }
    }
}
