﻿// -----------------------------------------------------------------------
// <copyright file="ProductbyClassificationResult.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MedicSupplier.Logic
{
    using System.Globalization;

    /// <summary>
    /// The result of ProductsAndOrdersbyClassification method.
    /// </summary>
    public class ProductbyClassificationResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProductbyClassificationResult"/> class.
        /// </summary>
        /// <param name="classification">The product's classficiation.</param>
        /// <param name="productID">the productID.</param>
        /// <param name="orderNumber">The number of orders by classificiations.</param>
        public ProductbyClassificationResult(string classification, int productID, int orderNumber)
        {
            this.Classification = classification;
            this.ProductID = productID;
            this.OrderNumber = orderNumber;
        }

        /// <summary>
        /// Gets or sets the classification.
        /// </summary>
        public string Classification { get; set; }

        /// <summary>
        /// Gets or sets the productID of the query.
        /// </summary>
        public int ProductID { get; set; }

        /// <summary>
        /// Gets or sets the number of orders by product.
        /// </summary>
        public int OrderNumber { get; set; }

        /// <summary>
        /// Returns the query's result in a string.
        /// </summary>
        /// <returns> The query's result in a string.</returns>
        public override string ToString()
        {
            return $"Classif:  {this.Classification}, ProductID: {this.ProductID}, Number of orders: {this.OrderNumber}";
        }

        /// <summary>
        /// Checks if two instances of results are equals or not.
        /// </summary>
        /// <param name="obj">The other result as object.</param>
        /// <returns>Returns true if the two are equal.</returns>
        public override bool Equals(object obj)
        {
            if (obj is ProductbyClassificationResult)
            {
                ProductbyClassificationResult other = obj as ProductbyClassificationResult;
                return this.Classification == other.Classification &&
                    this.ProductID == other.ProductID &&
                    this.OrderNumber == other.OrderNumber;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Returns the hashed value of the result using the three field.
        /// </summary>
        /// <returns>The hashcode of the result in integer.</returns>
        public override int GetHashCode()
        {
            return int.Parse(this.Classification, new CultureInfo("hu-HU")) + this.ProductID + this.OrderNumber;
        }
    }
}
