﻿// -----------------------------------------------------------------------
// <copyright file="IAdminLogic.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MedicSupplier.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using MedicSupplier.Data.Models;

    /// <summary>
    /// The logic interface for the administrative methods.
    /// </summary>
    public interface IAdminLogic
    {
        /// <summary>
        /// Add a new product the the database.
        /// </summary>
        /// <param name="manufacturerID">The id of the product's manufacturer.</param>
        /// <param name="name">The name of the product.</param>
        /// <param name="price">The price of the product.</param>
        /// <param name="inStock">The quantity in stock of the product.</param>
        /// <param name="reorder">The reorder level of the product.</param>
        /// <param name="classif">The classification of the product.</param>
        public void AddProduct(int manufacturerID, string name, double price, int inStock, int reorder, string classif);

        /// <summary>
        /// Deletes a product from the database.
        /// </summary>
        /// <param name="id">The id of the product.</param>
        public void DeleteProduct(int id);

        /// <summary>
        /// Updates one specific product's name field.
        /// </summary>
        /// <param name="id">The id of product.</param>
        /// <param name="name">The new name.</param>
        public void UpdateProductName(int id, string name);

        /// <summary>
        /// Add a new manufacturer the the database.
        /// </summary>
        /// <param name="name">The name of the manufacturer.</param>
        /// <param name="address">The address of the manufacturer.</param>
        /// <param name="country">The country of the manufacturer.</param>
        /// <param name="phone">The phone number of the manufacturer.</param>
        /// <param name="revenue">The revenue of the manufacturer.</param>
        /// <param name="email">The email of the manufacturer.</param>
        public void AddManufacturer(string name, string address, string country, string phone, double revenue, string email);

        /// <summary>
        /// Lists one specific manufacturer from the database.
        /// </summary>
        /// <param name="id">The id of the manufacturer.</param>
        /// <returns>The manufacturer object to list.</returns>
        public Manufacturer GetOneManufacturer(int id);

        /// <summary>
        /// Lists all manufacturers from the database.
        /// </summary>
        /// <returns>The manufacturers' list.</returns>
        public IList<Manufacturer> GetAllManufacturer();

        /// <summary>
        /// Deletes a manufacturer from the database.
        /// </summary>
        /// <param name="id">The id of the manufacturer.</param>
        public void DeleteManufacturer(int id);

        /// <summary>
        /// Updates one specific manufacturer's name field.
        /// </summary>
        /// <param name="id">The id of the manufacturer.</param>
        /// <param name="name">The new name.</param>
        public void UpdateManufacturerName(int id, string name);

        /// <summary>
        /// Updates one specific manufacturer's address field.
        /// </summary>
        /// <param name="id">The id of the manufacturer.</param>
        /// <param name="address">The new address.</param>
        public void UpdateManufacturerAddress(int id, string address);

        /// <summary>
        /// Updates one specific manufacturer's country field.
        /// </summary>
        /// <param name="id">The id of the manufacturer.</param>
        /// <param name="country">The new country.</param>
        public void UpdateManufacturerCountry(int id, string country);

        /// <summary>
        /// Updates one specific manufacturer's phone number field.
        /// </summary>
        /// <param name="id">The id of the manufacturer.</param>
        /// <param name="phoneNumber">The new phone number.</param>
        public void UpdateManufacturerPhone(int id, string phoneNumber);

        /// <summary>
        /// Updates one specific manufacturer's revenue field.
        /// </summary>
        /// <param name="id">The id of the manufacturer.</param>
        /// <param name="revenue">The new revenue.</param>
        public void UpdateManufacturerRevenue(int id, double revenue);

        /// <summary>
        /// Updates one specific manufacturer's email field.
        /// </summary>
        /// <param name="id">The id of the manufacturer.</param>
        /// <param name="email">The new email.</param>
        public void UpdateManufacturerEmail(int id, string email);

        /// <summary>
        /// Add a new customer the the database.
        /// </summary>
        /// <param name="name">The name of the customer.</param>
        /// <param name="address">The address of the customer.</param>
        /// <param name="country">The country of the customer.</param>
        /// <param name="phone">The phone of the customer.</param>
        /// <param name="email">The email of the customer.</param>
        public void AddCustomer(string name, string address, string country, string phone, string email);

        /// <summary>
        /// Lists one specific customer from the database.
        /// </summary>
        /// <param name="id">The id of the customer to be listed.</param>
        /// <returns>The customer object to list.</returns>
        public Customer GetOneCustomer(int id);

        /// <summary>
        /// Lists all customer from the database.
        /// </summary>
        /// <returns>The customers' list.</returns>
        public IList<Customer> GetAllCustomer();

        /// <summary>
        /// Deletes a customer from the database.
        /// </summary>
        /// <param name="id">The id of the customer.</param>
        public void DeleteCustomer(int id);

        /// <summary>
        /// Updates one specific customer's name field.
        /// </summary>
        /// <param name="id">The id of the customer.</param>
        /// <param name="name">The new name.</param>
        public void UpdateCustomerName(int id, string name);

        /// <summary>
        /// Updates one specific customer's address field.
        /// </summary>
        /// <param name="id">The id of the customer.</param>
        /// <param name="address">The new address.</param>
        public void UpdateCustomerAddress(int id, string address);

        /// <summary>
        /// Updates one specific customer's country field.
        /// </summary>
        /// <param name="id">The id of the customer.</param>
        /// <param name="country">The new country.</param>
        public void UpdateCustomerCountry(int id, string country);

        /// <summary>
        /// Updates one specific customer's phone number field.
        /// </summary>
        /// <param name="id">The id of the customer.</param>
        /// <param name="phoneNumber">The new phone number.</param>
        public void UpdateCustomerPhone(int id, string phoneNumber);

        /// <summary>
        /// Updates one specific customer's email field.
        /// </summary>
        /// <param name="id">The id of the customer.</param>
        /// <param name="email">The new email address.</param>
        public void UpdateCustomerEmail(int id, string email);

        /// <summary>
        /// Lists all the products from a country.
        /// </summary>
        /// <param name="country">The country to be used in the query.</param>
        /// <returns>The list of the products.</returns>
        public ICollection<Product> ListProductsByCountry(string country);

        /// <summary>
        /// Lists all the products from a country. Async version.
        /// </summary>
        /// <param name="country">The country to be used in the query.</param>
        /// <returns>The task of list of the products.</returns>
        public Task<ICollection<Product>> ListProductsByCountryAsync(string country);

        /// <summary>
        /// List all the manufacturers' country.
        /// </summary>
        /// <returns>The countries as list.</returns>
        public IQueryable<string> ListAllCountry();
    }
}
