﻿// -----------------------------------------------------------------------
// <copyright file="MedicSupplierContext.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;

[assembly: CLSCompliant(false)]

namespace MedicSupplier.Data.Models
{
    using System.Collections.Generic;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Represents the whole database.
    /// </summary>
    public class MedicSupplierContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MedicSupplierContext"/> class.
        /// </summary>
        public MedicSupplierContext()
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Gets or sets the customers in the database.
        /// </summary>
        public virtual DbSet<Customer> Customers { get; set; }

        /// <summary>
        /// Gets or sets the manufacturers in the database.
        /// </summary>
        public virtual DbSet<Manufacturer> Manufacturers { get; set; }

        /// <summary>
        /// Gets or sets the orders in the database.
        /// </summary>
        public virtual DbSet<Order> Orders { get; set; }

        /// <summary>
        /// Gets or sets the products in the database.
        /// </summary>
        public virtual DbSet<Product> Products { get; set; }

        /// <inheritdoc/>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder != null)
            {
                if (!optionsBuilder.IsConfigured)
                {
                    optionsBuilder.
                    UseLazyLoadingProxies().
                    UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\MedicDatabase.mdf;Integrated Security=True; MultipleActiveResultSets = true");
                }
            }
        }

        /// <inheritdoc/>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            if (modelBuilder != null)
            {
                // Creating the list of datas for the data seed.
                ICollection<Manufacturer> manufacturers = new List<Manufacturer>();
                ICollection<Customer> customers = new List<Customer>();
                ICollection<Product> products = new List<Product>();
                ICollection<Order> orders = new List<Order>();

                // Adding the manufacturers' datas.
                manufacturers.Add(new Manufacturer(1, "Cataflam", "Budapest, Lövői út 24", "Magyarország", "+36704503040", 1320000, "cataflam@gmail.com"));
                manufacturers.Add(new Manufacturer(2, "Bimbambucz kft.", "Budapest, Egressy Béni utca 45", "Magyarország", "+36302303434", 580340, "bimbambucz@gmail.com"));
                manufacturers.Add(new Manufacturer(3, "Aktoliflór bt.", "Budapest, Lövői út 24", "Magyarország", "+36707201203", 587887, "aktoliflor@gmail.com"));
                manufacturers.Add(new Manufacturer(4, "Medicamente", "Maulde, Jagerij 375", "Belgium", "+320471391211", 1100731, "medicamente@servais.be"));
                manufacturers.Add(new Manufacturer(5, "Rákospüspök kft.", "Veszprém, Teréz út 24", "Magyarország", "+36203435689", 838296, "rakospuspok@gmail.com"));
                manufacturers.Add(new Manufacturer(6, "Vörös és Társa bt.", "Budapest, Csabai kapu 46", "Magyarország", "+36309996750", 1071222, "vorosestarsa@gmail.com"));
                manufacturers.Add(new Manufacturer(7, "Van Hoof Associations", "Blaasveld, Pont du Chêne 301", "Belgium", "+320489386782", 3100731, "vanhoof@renard.be"));
                manufacturers.Add(new Manufacturer(8, "Barry Entreprise", "Hoboken, Place Fayat 84", "Belgium", "+320470705035", 1707027, "barry@servais.be"));
                manufacturers.Add(new Manufacturer(9, "Grupo Miranda", "Barcelona, Carrer de París 20", "Spanyolország", "+34771880752", 2555576, "grupo@miranda.sp"));
                manufacturers.Add(new Manufacturer(10, "Busch KG", "Berlin, Hallesche str. 80", "Németország", "+4909152434", 1909696, "busch@steffen.de"));

                // Adding the customers' datas.
                customers.Add(new Customer(1, "Zen Budapest", "Budapest, Vajas út 16", "Magyarország", "+3644121661", new DateTime(2020, 11, 03), "zenbp@gmail.com"));
                customers.Add(new Customer(2, "Buda Health", "Budapest, Bajza utca 4", "Magyarország", "+3648529076", new DateTime(2020, 09, 30), "budahp@gmail.com"));
                customers.Add(new Customer(3, "Kaleb bt.", "Pécs, Koller utca 24", "Magyarország", "+3625300416", new DateTime(2020, 11, 24), "kaleb@gmail.com"));
                customers.Add(new Customer(4, "Sok Egészség kft.", "Budapest, Városház utca 30", "Magyarország", "+3622188785", new DateTime(2020, 11, 24), "egeszseg@gmail.com"));
                customers.Add(new Customer(5, "Rael y Ros", "Villabrágima, Calvo Sotelo 43", "Spanyolország", "+34603958454", new DateTime(2020, 09, 30), "rael@guillen.com"));
                customers.Add(new Customer(6, "Förster Hildebrandt e.G.", "Berlin, Großbeerenstraße 18", "Németország", "+4930901180", new DateTime(2020, 10, 27), "förster@hildebrandt.de"));
                customers.Add(new Customer(7, "Bertram Betz AG", "Inning, Rankestraße 8", "Németország", "+4930905478", new DateTime(2020, 11, 17), "bertram@wetzel.com"));
                customers.Add(new Customer(8, "Charlier", "Proven, Rue du Vert Galant 285", "Belgium", "+320471235035", new DateTime(2020, 06, 30), "charlier@verbruggen.net"));
                customers.Add(new Customer(9, "Dogan NV", "Winterswijk, Prunusstraat 165", "Hollandia", "+310633546963", new DateTime(2020, 11, 10), "dogan@govarts.nl"));
                customers.Add(new Customer(10, "Alphaherb", "Győr, Vágóhíd utca 33", "Magyarország", "+3648578458", new DateTime(2020, 09, 22), "alphaherb@gmail.com"));
                customers.Add(new Customer(11, "Fischer AG", "Frankfurt, Ebersheimstraße 2", "Németország", "+4969458600", new DateTime(2020, 11, 03), "fischerag@gerber.net"));

                // Adding the products' datas.
                products.Add(new Product(1, 1, "Silvatant", 300.85, 80, 200, "2"));
                products.Add(new Product(2, 4, "Dextrostryl", 900.14, 551, 400, "1"));
                products.Add(new Product(3, 3, "Avasonide", 600, 369, 100, "1"));
                products.Add(new Product(4, 7, "Prenide", 100.79, 938, 500, "3"));
                products.Add(new Product(5, 6, "Ramitecan", 200.5, 242, 300, "2"));
                products.Add(new Product(6, 5, "Alitrelinum", 300, 1064, 500, "2"));
                products.Add(new Product(7, 4, "Ablafenide Artetestryl", 700.75, 160, 100, "1"));
                products.Add(new Product(8, 9, "Pentorix Cordarall", 600.23, 460, 150, "1"));
                products.Add(new Product(9, 7, "Sensiferol", 900, 365, 350, "1"));
                products.Add(new Product(10, 7, "Nitrotinoin", 500.5, 348, 300, "1"));
                products.Add(new Product(11, 10, "Aplenprex", 300.43, 230, 100, "2"));
                products.Add(new Product(12, 2, "Formolan", 800.99, 560, 250, "1"));
                products.Add(new Product(13, 5, "Entraracin Rivamune", 500.64, 600, 500, "1"));
                products.Add(new Product(14, 9, "Morphinazole", 400.25, 600, 500, "2"));
                products.Add(new Product(15, 9, "Acytroban", 500.45, 40, 100, "1"));
                products.Add(new Product(16, 6, "Gadoprodol", 300.55, 200, 100, "2"));
                products.Add(new Product(17, 7, "Ramibamol Zanaracin", 400, 750, 500, "2"));
                products.Add(new Product(18, 3, "Asparasone Fibriluble", 400.25, 130, 100, "2"));
                products.Add(new Product(19, 8, "Nitrolinum Tacrotora", 400, 1470, 200, "2"));
                products.Add(new Product(20, 8, "Requizolam", 800.5, 141, 100, "1"));
                products.Add(new Product(21, 7, "Ganimara", 900.5, 427, 300, "1"));
                products.Add(new Product(22, 10, "Sensixitrol", 100.25, 804, 500, "3"));
                products.Add(new Product(23, 2, "Halcixane", 300.5, 355, 200, "2"));
                products.Add(new Product(24, 10, "Agevid", 400.99, 433, 200, "2"));
                products.Add(new Product(25, 1, "Ambesine Icotrace", 100.99, 900, 200, "3"));

                // Adding the orders' datas.
                orders.Add(new Order(1, 2, 8, 200, new DateTime(2020, 12, 31)));
                orders.Add(new Order(2, 4, 9, 100, new DateTime(2020, 12, 11)));
                orders.Add(new Order(3, 7, 1, 75, new DateTime(2020, 12, 18)));
                orders.Add(new Order(4, 6, 7, 300, new DateTime(2020, 12, 18)));
                orders.Add(new Order(5, 15, 6, 40, new DateTime(2020, 12, 18)));
                orders.Add(new Order(6, 14, 2, 50, new DateTime(2020, 12, 03)));
                orders.Add(new Order(7, 20, 5, 20, new DateTime(2020, 12, 11)));
                orders.Add(new Order(8, 9, 4, 50, new DateTime(2020, 12, 11)));
                orders.Add(new Order(9, 17, 3, 400, new DateTime(2021, 01, 29)));
                orders.Add(new Order(10, 1, 10, 20, new DateTime(2021, 01, 29)));

                // Setting up relations between tables.
                modelBuilder.Entity<Product>(entity =>
                {
                    entity.HasOne(product => product.Manufacturer).
                    WithMany(manufacturer => manufacturer.Products).
                    HasForeignKey(product => product.ManufacturerID).
                    OnDelete(DeleteBehavior.Cascade);
                });

                modelBuilder.Entity<Order>(entity =>
                {
                    entity.HasOne(order => order.Customer).
                    WithMany(customer => customer.Orders).
                    HasForeignKey(order => order.CustomerID).
                    OnDelete(DeleteBehavior.Cascade);
                });

                modelBuilder.Entity<Order>(entity =>
                {
                    entity.HasOne(order => order.Product).
                    WithMany(product => product.Orders).
                    HasForeignKey(order => order.ProductID).
                    OnDelete(DeleteBehavior.Cascade);
                });

                // Adding the data seed to the database.
                modelBuilder.Entity<Manufacturer>().HasData(manufacturers);
                modelBuilder.Entity<Customer>().HasData(customers);
                modelBuilder.Entity<Product>().HasData(products);
                modelBuilder.Entity<Order>().HasData(orders);

                // SaveChanges();
            }
        }
    }
}
