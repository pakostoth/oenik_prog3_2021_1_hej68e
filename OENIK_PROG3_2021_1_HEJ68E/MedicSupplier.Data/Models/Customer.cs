﻿// -----------------------------------------------------------------------
// <copyright file="Customer.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MedicSupplier.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Globalization;

    /// <summary>
    /// Represents a customer company in the database.
    /// </summary>
    [Table("Customer")]
    public class Customer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Customer"/> class.
        /// This is used in data seeding.
        /// </summary>
        /// <param name="customerID">Datebase-generated id.</param>
        /// <param name="name">The name of the customer.</param>
        /// <param name="address">The address of the manufacturer.</param>
        /// <param name="country">The country of the manufacturer.</param>
        /// <param name="phoneNumber">The phone number of the office.</param>
        /// <param name="lastOrderDate">The last order's date.</param>
        /// <param name="email">E-mail, probably HR's.</param>
        public Customer(
            int customerID,
            string name,
            string address,
            string country,
            string phoneNumber,
            DateTime lastOrderDate,
            string email)
        {
            this.CustomerID = customerID;
            this.Name = name;
            this.Address = address;
            this.Country = country;
            this.PhoneNumber = phoneNumber;
            this.LastOrderDate = lastOrderDate;
            this.Email = email;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Customer"/> class.
        /// This is the default constructor when adding an entity.
        /// </summary>
        /// <param name="name">The name of the customer.</param>
        /// <param name="address">The address of the manufacturer.</param>
        /// <param name="country">The country of the manufacturer.</param>
        /// <param name="phoneNumber">The phone number of the office.</param>
        /// <param name="lastOrderDate">The last order's date.</param>
        /// <param name="email">E-mail, probably HR's.</param>
        public Customer(
            string name,
            string address,
            string country,
            string phoneNumber,
            DateTime lastOrderDate,
            string email)
        {
            this.Name = name;
            this.Address = address;
            this.Country = country;
            this.PhoneNumber = phoneNumber;
            this.LastOrderDate = lastOrderDate;
            this.Email = email;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Customer"/> class.
        /// The empty constructor.
        /// </summary>
        public Customer()
        {
        }

        /// <summary>
        /// Gets or sets the customer's ID.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CustomerID { get; set; }

        /// <summary>
        /// Gets the customer's order lists.
        /// Navigation property, not mapped.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Order> Orders { get; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the phone number.
        /// </summary>
        [Required]
        [MaxLength(20)]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Gets or sets the last order's date.
        /// </summary>
        [Required]
        public DateTime LastOrderDate { get; set; }

        /// <summary>
        /// Gets or sets the customer's email address.
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Email { get; set; }

        /// <summary>
        /// The basic toString method using all data.
        /// </summary>
        /// <returns>All data as a string.</returns>
        public override string ToString()
        {
            // About how this works:
            // I give the forFormat the data names,
            // then the format method use this and the actual datas
            // to give back a string with all the data formatted.
            // Mostly used when writing out one entity.
            string[] forFormat = { "ID", "Name:", "Address:", "Country:", "Phone number:", "Last order date:", "Email:" };
            return string.Format(
                new CultureInfo("hu-HU"),
                "{7,-30}{0}\n{8,-30}{1}\n{9,-30}{2}\n{10,-30}{3}\n{11,-30}{4}\n{12,-30}{5}\n{13,-30}{6}",
                this.CustomerID,
                this.Name,
                this.Address,
                this.Country,
                this.PhoneNumber,
                this.LastOrderDate.ToShortDateString(),
                this.Email,
                forFormat[0],
                forFormat[1],
                forFormat[2],
                forFormat[3],
                forFormat[4],
                forFormat[5],
                forFormat[6]);
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is Customer)
            {
                Customer other = obj as Customer;
                return this.CustomerID == other.CustomerID &&
                    this.Name == other.Name &&
                    this.Address == other.Address &&
                    this.Country == other.Country &&
                    this.PhoneNumber == other.PhoneNumber &&
                    this.LastOrderDate == other.LastOrderDate &&
                    this.Email == other.Email;
            }

            return false;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.CustomerID + this.Name.GetHashCode(0);
        }
    }
}
