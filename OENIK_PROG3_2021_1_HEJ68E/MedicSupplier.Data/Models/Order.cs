﻿// -----------------------------------------------------------------------
// <copyright file="Order.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MedicSupplier.Data.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Globalization;

    /// <summary>
    /// Represents an order.
    /// </summary>
    [Table("Order")]
    public class Order
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Order"/> class.
        /// This is used in data seeding.
        /// </summary>
        /// <param name="orderID">Datebase-generated id.</param>
        /// <param name="productID">The id of the ordered product.</param>
        /// <param name="customerID">The id of the ordering customer.</param>
        /// <param name="quantity">The quantity of the ordered product.</param>
        /// <param name="deadline">The date until you have time to procastinate.</param>
        public Order(
            int orderID,
            int productID,
            int customerID,
            int quantity,
            DateTime deadline)
        {
            this.OrderID = orderID;
            this.ProductID = productID;
            this.CustomerID = customerID;
            this.Quantity = quantity;
            this.Deadline = deadline;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Order"/> class.
        /// This is the default constructor when adding an entity.
        /// </summary>
        /// <param name="productID">The id of the ordered product.</param>
        /// <param name="customerID">The id of the ordering customer.</param>
        /// <param name="quantity">The quantity of the ordered product.</param>
        /// <param name="deadline">The date until you have time to procastinate.</param>
        public Order(
            int productID,
            int customerID,
            int quantity,
            DateTime deadline)
        {
            this.ProductID = productID;
            this.CustomerID = customerID;
            this.Quantity = quantity;
            this.Deadline = deadline;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Order"/> class.
        /// The empty constructor.
        /// </summary>
        public Order()
        {
        }

        /// <summary>
        /// Gets or sets the id of the order.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrderID { get; set; }

        /// <summary>
        /// Gets or sets the product reference in the order.
        /// Navigation property, not mapped.
        /// </summary>
        [NotMapped]
        public virtual Product Product { get; set; }

        /// <summary>
        /// Gets or sets the order's product ID.
        /// There is only one product per order.
        /// </summary>
        [ForeignKey("Product")]
        public int ProductID { get; set; }

        /// <summary>
        /// Gets or sets the customer reference in the order.
        /// Navigation property, not mapped.
        /// </summary>
        [NotMapped]
        public virtual Customer Customer { get; set; }

        /// <summary>
        /// Gets or sets the order's customer ID.
        /// There is only one customer per order.
        /// </summary>
        [Required]
        [ForeignKey("Customer")]
        public int CustomerID { get; set; }

        /// <summary>
        /// Gets or sets the ordered product's quantity.
        /// </summary>
        [Required]
        public int Quantity { get; set; }

        /// <summary>
        /// Gets or sets the order's deadline.
        /// </summary>
        [Required]
        public DateTime Deadline { get; set; }

        /// <summary>
        /// The basic toString method using all data.
        /// </summary>
        /// <returns>All data as a string.</returns>
        public override string ToString()
        {
            // About how this works:
            // I give the forFormat the data names,
            // then the format method use this and the actual datas
            // to give back a string with all the data formatted.
            // Mostly used when writing out one entity.
            string[] forFormat = { "ID", "Product name:", "Customer name:", "Quantity:", "Deadline:" };
            return this.OrderID == default ? string.Empty : string.Format(
                new CultureInfo("hu-HU"),
                "{5,-30}{0}\n{6,-30}{1}\n{7,-30}{2}\n{8,-30}{3}\n{9,-30}{4}",
                this.CustomerID,
                this.Product.Name,
                this.Customer.Name,
                this.Quantity,
                this.Deadline.ToShortDateString(),
                forFormat[0],
                forFormat[1],
                forFormat[2],
                forFormat[3],
                forFormat[4]);
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is Order)
            {
                Order other = obj as Order;
                return this.CustomerID == other.CustomerID &&
                    this.ProductID == other.ProductID &&
                    this.CustomerID == other.CustomerID &&
                    this.Quantity == other.Quantity &&
                    this.Deadline == other.Deadline;
            }

            return false;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.OrderID + this.CustomerID + this.ProductID;
        }
    }
}
