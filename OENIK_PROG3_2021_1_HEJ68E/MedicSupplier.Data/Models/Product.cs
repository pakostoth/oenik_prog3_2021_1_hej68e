﻿// -----------------------------------------------------------------------
// <copyright file="Product.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MedicSupplier.Data.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Globalization;

    /// <summary>
    /// Represents a product in the database.
    /// </summary>
    [Table("Product")]
    public class Product
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Product"/> class.
        /// This is used in data seeding.
        /// </summary>
        /// <param name="productID">Datebase-generated id.</param>
        /// <param name="manufacturerID">Foregin key, for the product's manufacturer.</param>
        /// <param name="name">The name of the product.</param>
        /// <param name="price">The price of the product, stored in double.</param>
        /// <param name="inStock">The available quantity of the product in stock.</param>
        /// <param name="reorderLevel">The quantity when you have to order again.</param>
        /// <param name="classification">A number for how important the product. Higher number means more important.</param>
        public Product(
            int productID,
            int manufacturerID,
            string name,
            double price,
            int inStock,
            int reorderLevel,
            string classification)
        {
            this.ProductID = productID;
            this.ManufacturerID = manufacturerID;
            this.Name = name;
            this.Price = price;
            this.InStock = inStock;
            this.ReorderLevel = reorderLevel;
            this.Classification = classification;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Product"/> class.
        /// This is the default constructor when adding an entity.
        /// </summary>
        /// <param name="manufacturerID">Foregin key, for the product's manufacturer.</param>
        /// <param name="name">The name of the product.</param>
        /// <param name="price">The price of the product, stored in double.</param>
        /// <param name="inStock">The available quantity of the product in stock.</param>
        /// <param name="reorderLevel">The quantity when you have to order again.</param>
        /// <param name="classification">A number for how important the product. Higher number means more important.</param>
        public Product(
            int manufacturerID,
            string name,
            double price,
            int inStock,
            int reorderLevel,
            string classification)
        {
            this.ManufacturerID = manufacturerID;
            this.Name = name;
            this.Price = price;
            this.InStock = inStock;
            this.ReorderLevel = reorderLevel;
            this.Classification = classification;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Product"/> class.
        /// The empty constructor.
        /// </summary>
        public Product()
        {
        }

        /// <summary>
        /// Gets or sets the ID number of the product.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProductID { get; set; }

        /// <summary>
        /// Gets or sets the manufacturer's ID at the product table.
        /// </summary>
        [ForeignKey("Manufacturer")]
        public int ManufacturerID { get; set; }

        /// <summary>
        /// Gets the product's manufacturer.
        /// Navigation property, not mapped.
        /// </summary>
        [NotMapped]
        public virtual Manufacturer Manufacturer { get; }

        /// <summary>
        /// Gets the product's all orders.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Order> Orders { get; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        [Required]
        public double Price { get; set; }

        /// <summary>
        /// Gets or sets the available quantity.
        /// </summary>
        [Required]
        public int InStock { get; set; }

        /// <summary>
        /// Gets or sets the level when to order again.
        /// </summary>
        [Required]
        public int ReorderLevel { get; set; }

        /// <summary>
        /// Gets or sets the classification.
        /// </summary>
        [Required]
        [MaxLength(2)]
        public string Classification { get; set; }

        /// <summary>
        /// The basic toString method using all data.
        /// </summary>
        /// <returns>All data as a string.</returns>
        public override string ToString()
        {
            // About how this works:
            // I give the forFormat the data names,
            // then the format method use this and the actual datas
            // to give back a string with all the data formatted.
            // Mostly used when writing out one entity.
            string[] forFormat = { "ID", "Manufacturer name:", "Name:", "Price:", "Quantity in stock:", "Reorder level:", "Classification:" };
            return string.Format(
                new CultureInfo("hu-HU"),
                "{7,-30}{0}\n{8,-30}{1}\n{9,-30}{2}\n{10,-30}{3:C}\n{11,-30}{4}\n{12,-30}{5}\n{13,-30}{6}",
                this.ProductID,
                this.Manufacturer.Name,
                this.Name,
                this.Price,
                this.InStock,
                this.ReorderLevel,
                this.Classification,
                forFormat[0],
                forFormat[1],
                forFormat[2],
                forFormat[3],
                forFormat[4],
                forFormat[5],
                forFormat[6]);
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is Product)
            {
                Product other = obj as Product;
                return this.ProductID == other.ProductID &&
                    this.Name == other.Name &&
                    this.Price == other.Price &&
                    this.InStock == other.InStock &&
                    this.ReorderLevel == other.ReorderLevel &&
                    this.Classification == other.Classification;
            }

            return false;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.ProductID + this.Name.GetHashCode(0);
        }
    }
}
