﻿// -----------------------------------------------------------------------
// <copyright file="Manufacturer.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MedicSupplier.Data.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Globalization;

    /// <summary>
    /// Represents a manufacturer company in the database.
    /// </summary>
    [Table("Manufacturer")]
    public class Manufacturer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Manufacturer"/> class.
        /// This is used in data seeding.
        /// </summary>
        /// <param name="manufacturerID">Database-generated id.</param>
        /// <param name="name">The name of the manufacturer.</param>
        /// <param name="address">The address of the manufacturer.</param>
        /// <param name="country">The country of the manufacturer.</param>
        /// <param name="phoneNumber">The phone number of the office.</param>
        /// <param name="revenue">Last year's revenue.</param>
        /// <param name="email">E-mail, probably HR's.</param>
        public Manufacturer(
            int manufacturerID,
            string name,
            string address,
            string country,
            string phoneNumber,
            double revenue,
            string email)
        {
            this.ManufacturerID = manufacturerID;
            this.Name = name;
            this.Address = address;
            this.Country = country;
            this.PhoneNumber = phoneNumber;
            this.Revenue = revenue;
            this.Email = email;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Manufacturer"/> class.
        /// This is the default constructor when adding an entity.
        /// </summary>
        /// <param name="name">The name of the manufacturer.</param>
        /// <param name="address">The address of the manufacturer.</param>
        /// <param name="country">The country of the manufacturer.</param>
        /// <param name="phoneNumber">The phone number of the office.</param>
        /// <param name="revenue">Last year's revenue.</param>
        /// <param name="email">E-mail, probably HR's.</param>
        public Manufacturer(
            string name,
            string address,
            string country,
            string phoneNumber,
            double revenue,
            string email)
        {
            this.Name = name;
            this.Address = address;
            this.Country = country;
            this.PhoneNumber = phoneNumber;
            this.Revenue = revenue;
            this.Email = email;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Manufacturer"/> class.
        /// The empty constructor.
        /// </summary>
        public Manufacturer()
        {
        }

        /// <summary>
        /// Gets or sets the manufacturer's ID.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ManufacturerID { get; set; }

        /// <summary>
        /// Gets the manufacturer's products.
        /// Navigation property, not mapped.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Product> Products { get; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the phone number.
        /// </summary>
        [Required]
        [MaxLength(20)]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Gets or sets the revenue.
        /// </summary>
        [Required]
        [MaxLength(30)]
        public double Revenue { get; set; }

        /// <summary>
        /// Gets or sets the manufacturer's email address.
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Email { get; set; }

        /// <summary>
        /// The basic toString method using all data.
        /// </summary>
        /// <returns>All data as a string.</returns>
        public override string ToString()
        {
            // About how this works:
            // I give the forFormat the data names,
            // then the format method use this and the actual datas
            // to give back a string with all the data formatted.
            // Mostly used when writing out one entity.
            string[] forFormat = { "ID", "Name:", "Address:", "Country:", "Phone number:", "Revenue:", "Email:" };
            return string.Format(
                new CultureInfo("hu-HU"),
                "{7,-30}{0}\n{8,-30}{1}\n{9,-30}{2}\n{10,-30}{3}\n{11,-30}{4}\n{12,-30}{5:C}\n{13,-30}{6}",
                this.ManufacturerID,
                this.Name,
                this.Address,
                this.Country,
                this.PhoneNumber,
                this.Revenue,
                this.Email,
                forFormat[0],
                forFormat[1],
                forFormat[2],
                forFormat[3],
                forFormat[4],
                forFormat[5],
                forFormat[6]);
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is Manufacturer)
            {
                Manufacturer other = obj as Manufacturer;
                return this.ManufacturerID == other.ManufacturerID &&
                    this.Name == other.Name &&
                    this.Address == other.Address &&
                    this.Country == other.Country &&
                    this.PhoneNumber == other.PhoneNumber &&
                    this.Revenue == other.Revenue &&
                    this.Email == other.Email;
            }

            return false;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.ManufacturerID + this.Name.GetHashCode(0);
        }
    }
}
