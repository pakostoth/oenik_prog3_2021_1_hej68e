﻿// -----------------------------------------------------------------------
// <copyright file="MenuUICRDMethods.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MedicSupplier.Program
{
    using System;
    using System.Linq;
    using System.Net.Mail;
    using System.Text.RegularExpressions;
    using System.Threading;
    using MedicSupplier.Logic;

    /// <summary>
    /// Represents the class responsible for the user interface.
    /// </summary>
    public partial class MenuUI
    {
        /// <summary>
        /// Lists all customers to the console.
        /// </summary>
        public void ListAllCustomer()
        {
            string value = "All Customers";
            Console.Clear();
            System.Console.WriteLine($"--------------------\n{0}\n--------------------", value);
            System.Console.WriteLine(string.Format(Info, "{0,-5}{1,-30}{2,-40}{3,-20}{4,-20}{5,-20}{6,-30}", "ID", "Name", "Address", "Country", "Phone number", "Last order date", "Email"));
            Console.WriteLine();
            var result = this.Factory.AdminLogic
                .GetAllCustomer()
                .ToList();
            Thread.Sleep(300);

            foreach (var x in result)
            {
                System.Console.WriteLine(
                    string.Format(Info, "{0,-5}{1,-30}{2,-40}{3,-20}{4,-20}{5,-20}{6,-30}", x.CustomerID, x.Name, x.Address, x.Country, x.PhoneNumber, x.LastOrderDate.ToShortDateString(), x.Email));
                Thread.Sleep(100);
            }

            Thread.Sleep(300);
            QueryComplete();
        }

        /// <summary>
        /// Lists all manufacturers to the console.
        /// </summary>
        public void ListAllManufacturer()
        {
            string value = "All Manufacturers";
            Console.Clear();
            System.Console.WriteLine($"--------------------\n{0}\n--------------------", value);
            System.Console.WriteLine(string.Format(Info, "{0,-5}{1,-30}{2,-40}{3,-20}{4,-20}{5,-20}{6,-30}", "ID", "Name", "Address", "Country", "Phone number", "Revenue", "Email"));
            Console.WriteLine();
            var result = this.Factory.AdminLogic
                .GetAllManufacturer()
                .ToList();
            Thread.Sleep(300);

            foreach (var x in result)
            {
                System.Console.WriteLine(
                    string.Format(Info, "{0,-5}{1,-30}{2,-40}{3,-20}{4,-20}{5,-20}{6,-30}", x.ManufacturerID, x.Name, x.Address, x.Country, x.PhoneNumber, x.Revenue.ToString(Info), x.Email));
                Thread.Sleep(100);
            }

            Thread.Sleep(300);
            QueryComplete();
        }

        /// <summary>
        /// Lists all products to the console.
        /// </summary>
        public void ListAllProduct()
        {
            string value = "All Products";
            Console.Clear();
            System.Console.WriteLine($"--------------------\n{0}\n--------------------", value);
            System.Console.WriteLine(string.Format(Info, "{0,-5}{1,-30}{2,-40}{3,-10}{4,-20}{5,-20}{6,-20}", "ID", "Manufacturer Name", "Product name", "Price", "Quantity in stock", "Reorder level", "Classification"));
            Console.WriteLine();
            var result = this.Factory.OrderLogic
                .GetAllProduct()
                .ToList();
            Thread.Sleep(300);

            foreach (var x in result)
            {
                System.Console.WriteLine(
                    string.Format(Info, "{0,-5}{1,-30}{2,-40}{3,-10}{4,-20}{5,-20}{6,-20}", x.ProductID, x.Manufacturer.Name, x.Name, x.Price, x.InStock.ToString(Info), x.ReorderLevel.ToString(Info), x.Classification));
                Thread.Sleep(100);
            }

            Thread.Sleep(300);
            QueryComplete();
        }

        /// <summary>
        /// Lists all orders to the console.
        /// </summary>
        public void ListAllOrder()
        {
            string value = "All Orders";
            Console.Clear();
            System.Console.WriteLine($"--------------------\n{0}\n--------------------", value);
            System.Console.WriteLine(string.Format(Info, "{0,-10}{1,-30}{2,-40}{3,-20}{4,-20}", "Order ID", "Product name", "Customer name", "Quantity", "Deadline"));
            Console.WriteLine();
            var result = this.Factory.OrderLogic
                .GetAllOrder()
                .ToList();
            Thread.Sleep(300);

            foreach (var x in result)
            {
                System.Console.WriteLine(
                    string.Format(Info, "{0,-10}{1,-30}{2,-40}{3,-20}{4,-20}", x.OrderID, x.Product.Name, x.Customer.Name, x.Quantity, x.Deadline.ToShortDateString()));
                Thread.Sleep(100);
            }

            Thread.Sleep(300);
            QueryComplete();
        }

        /// <summary>
        /// Lists one customer to the console.
        /// </summary>
        public void ListOneCustomer()
        {
            int id = this.CustomerIDValidation();
            Console.WriteLine(this.Factory.AdminLogic.GetOneCustomer(id).ToString());
            Console.ReadKey();
        }

        /// <summary>
        /// Lists one manufacturer to the console.
        /// </summary>
        public void ListOneManufacturer()
        {
            int id = this.ManufacturerIDValidation();
            Console.WriteLine(this.Factory.AdminLogic.GetOneManufacturer(id).ToString());
            Console.ReadKey();
        }

        /// <summary>
        /// Lists one product to the console.
        /// </summary>
        public void ListOneProduct()
        {
            int id = this.ProductIDValidation();
            Console.WriteLine(this.Factory.OrderLogic.GetOneProduct(id).ToString());
            Console.ReadKey();
        }

        /// <summary>
        /// Lists one order to the console.
        /// </summary>
        public void ListOneOrder()
        {
            int id = this.ProductIDValidation();
            Console.WriteLine(this.Factory.OrderLogic.GetOneOrder(id).ToString());
            Console.ReadKey();
        }

        /// <summary>
        /// Add a new customer using the console menu.
        /// </summary>
        public void AddCustomer()
        {
            // this.AdminLogic.GetOneCustomer(1).GetType().GetProperties().Where( x => x.GetCustomAttribute<DataAttribute>() != null)
            string entity = "customer";
            Console.WriteLine($"Adding one {entity}");
            Console.WriteLine($"Please enter the name:");
            string name;
            bool cycle = true;
            do
            {
                name = Console.ReadLine();
                if (name.Length < 50)
                {
                    cycle = false;
                }
                else
                {
                    Console.WriteLine("Too long name!");
                }
            }
            while (cycle);
            Console.WriteLine($"Please enter the address:");
            string address;
            cycle = true;
            do
            {
                address = Console.ReadLine();
                if (address.Length < 50)
                {
                    cycle = false;
                }
                else
                {
                    Console.WriteLine("Too long address!");
                }
            }
            while (cycle);
            Console.WriteLine($"Please enter the country:");
            string country;
            cycle = true;
            do
            {
                country = Console.ReadLine();
                if (country.Length < 50)
                {
                    cycle = false;
                }
                else
                {
                    Console.WriteLine("Too long name!");
                }
            }
            while (cycle);
            Console.WriteLine($"Please enter the phone: (just the numbers)");
            string phone;
            cycle = true;
            do
            {
                phone = Console.ReadLine();
                if (IsPhoneNumber(phone))
                {
                    cycle = false;
                }
                else
                {
                    Console.WriteLine("Incorrect phone number!");
                }
            }
            while (cycle);
            Console.WriteLine($"Please enter the email:");
            string email = string.Empty;
            cycle = true;
            do
            {
                string localEmail = Console.ReadLine();
                try
                {
                    email = new MailAddress(localEmail).Address;
                    cycle = false;
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            while (cycle);
            this.AdminLogic.AddCustomer(name, address, country, phone, email);
            Console.WriteLine("Customer added");
            Console.ReadKey();
        }

        /// <summary>
        /// Add a new manufacturer using the console menu.
        /// </summary>
        public void AddManufacturer()
        {
            string entity = "manufacturer";
            Console.WriteLine($"Adding one {entity}");
            Console.WriteLine($"Please enter the name:");
            string name;
            bool cycle = true;
            do
            {
                name = Console.ReadLine();
                if (name.Length < 50)
                {
                    cycle = false;
                }
                else
                {
                    Console.WriteLine("Too long name!");
                }
            }
            while (cycle);
            Console.WriteLine($"Please enter the address:");
            string address;
            cycle = true;
            do
            {
                address = Console.ReadLine();
                if (address.Length < 50)
                {
                    cycle = false;
                }
                else
                {
                    Console.WriteLine("Too long address!");
                }
            }
            while (cycle);
            Console.WriteLine($"Please enter the country:");
            string country;
            cycle = true;
            do
            {
                country = Console.ReadLine();
                if (country.Length < 50)
                {
                    cycle = false;
                }
                else
                {
                    Console.WriteLine("Too long name!");
                }
            }
            while (cycle);
            Console.WriteLine($"Please enter the phone:");
            string phone;
            cycle = true;
            do
            {
                phone = Console.ReadLine();
                if (IsPhoneNumber(phone))
                {
                    cycle = false;
                }
                else
                {
                    Console.WriteLine("Incorrect phone number!");
                }
            }
            while (cycle);
            Console.WriteLine($"Please enter the revenue:");
            double revenue = 0;
            cycle = true;
            do
            {
                try
                {
                    string local = Console.ReadLine();
                    revenue = IsValidDouble(local);
                    if (revenue > 0)
                    {
                        cycle = false;
                    }
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }

                if (cycle)
                {
                    Console.WriteLine("Invalid number");
                }
            }
            while (cycle);
            Console.WriteLine($"Please enter the email:");
            string email = string.Empty;
            cycle = false;
            do
            {
                string localEmail = Console.ReadLine();
                try
                {
                    email = new MailAddress(localEmail).Address;
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            while (cycle);
            this.AdminLogic.AddManufacturer(name, address, country, phone, revenue, email);
            Console.WriteLine("Manufacturer added");
            Console.ReadKey();
        }

        /// <summary>
        /// Add a new product using the console menu.
        /// </summary>
        public void AddProduct()
        {
            Console.WriteLine("Adding one product");
            Console.WriteLine("Please enter the manufacturer's ID:");
            int manufacturerID = this.ManufacturerIDValidation();
            Console.WriteLine("Please enter the name:");
            string name;
            bool cycle = true;
            do
            {
                name = Console.ReadLine();
                if (name.Length < 50)
                {
                    cycle = false;
                }
                else
                {
                    Console.WriteLine("Too long name!");
                }
            }
            while (cycle);
            Console.WriteLine("Please enter the price:");
            double price = 0;
            cycle = true;
            do
            {
                try
                {
                    string local = Console.ReadLine();
                    price = IsValidDouble(local);
                    if (price > 0)
                    {
                        cycle = false;
                    }
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }

                if (cycle)
                {
                    Console.WriteLine("Invalid number");
                }
            }
            while (cycle);
            Console.WriteLine("Please enter the quantity in stock:");
            int inStock = 0;
            cycle = true;
            do
            {
                try
                {
                    string local = Console.ReadLine();
                    inStock = IsValidInt(local);
                    if (inStock > 0)
                    {
                        cycle = false;
                    }
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }

                if (cycle)
                {
                    Console.WriteLine("Invalid number");
                }
            }
            while (cycle);
            Console.WriteLine("Please enter the reorder level:");
            int reorder = 0;
            cycle = true;
            do
            {
                try
                {
                    string local = Console.ReadLine();
                    reorder = IsValidInt(local);
                    if (reorder > 0)
                    {
                        cycle = false;
                    }
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }

                if (cycle)
                {
                    Console.WriteLine("Invalid number");
                }
            }
            while (cycle);
            Console.WriteLine("Please enter the classification:");
            string classif;
            cycle = true;
            do
            {
                classif = Console.ReadLine();
                if (classif.Length < 3 && Regex.IsMatch(classif, @"^[0-9]+$"))
                {
                    cycle = false;
                }
                else
                {
                    Console.WriteLine("Too long classification!");
                }
            }
            while (cycle);
            this.AdminLogic.AddProduct(manufacturerID, name, price, inStock, reorder, classif);
            Console.WriteLine("Product added");
            Console.ReadKey();
        }

        /// <summary>
        /// Add a new order using the console menu.
        /// </summary>
        public void AddOrder()
        {
            Console.WriteLine("Write the new order:");
            Console.WriteLine("Please enter the product's ID:");
            int productID = this.ProductIDValidation();
            Console.WriteLine("Please enter the customer's ID:");
            int customerID = this.CustomerIDValidation();
            Console.WriteLine("Please enter the quantity:");
            int quantity = 0;
            bool cycle = true;
            do
            {
                try
                {
                    string local = Console.ReadLine();
                    quantity = IsValidInt(local);
                    if (quantity > 0)
                    {
                        cycle = false;
                    }
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }

                if (cycle)
                {
                    Console.WriteLine("Invalid number");
                }
            }
            while (cycle);
            Console.WriteLine("Write the new deadline:");
            DateTime deadline = DateTime.MinValue;
            cycle = true;
            do
            {
                string date = Console.ReadLine();
                if (DateTime.TryParse(date, out DateTime result))
                {
                    deadline = result;
                    cycle = false;
                }
                else
                {
                    Console.WriteLine("Incorrect deadline! Try again!");
                }
            }
            while (cycle);
            try
            {
                this.OrderLogic.AddOrder(productID, customerID, quantity, deadline);
            }
            catch (TooFewInStockException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey();
                return;
            }

            Console.WriteLine("Order added");
            Console.ReadKey();
        }

        /// <summary>
        /// Delete a customer using the console menu.
        /// </summary>
        public void DeleteCustomer()
        {
            this.ListAllCustomer();
            int id = this.CustomerIDValidation();
            this.AdminLogic.DeleteCustomer(id);
            Console.WriteLine();
            Console.WriteLine("Delete complete");
            this.ListAllCustomer();
        }

        /// <summary>
        /// Delete a manufacturer using the console menu.
        /// </summary>
        public void DeleteManufacturer()
        {
            this.ListAllManufacturer();
            int id = this.ManufacturerIDValidation();
            this.AdminLogic.DeleteManufacturer(id);
            Console.WriteLine();
            Console.WriteLine("Delete complete");
            this.ListAllManufacturer();
        }

        /// <summary>
        /// Delete a product using the console menu.
        /// </summary>
        public void DeleteProduct()
        {
            this.ListAllProduct();
            int id = this.ProductIDValidation();
            this.AdminLogic.DeleteProduct(id);
            Console.WriteLine();
            Console.WriteLine("Delete complete");
            this.ListAllProduct();
        }

        /// <summary>
        /// Delete a order using the console menu.
        /// </summary>
        public void DeleteOrder()
        {
            this.ListAllOrder();
            int id = this.OrderIDValidation();
            this.OrderLogic.DeleteOrder(id);
            Console.WriteLine();
            Console.WriteLine("Delete complete");
            this.ListAllOrder();
        }

        /// <summary>
        /// Validate the id from the console.
        /// </summary>
        /// <returns>The id as an integer.</returns>
        private int CustomerIDValidation()
        {
            Console.WriteLine();
            Console.WriteLine("Write the ID: ");
            bool badNumber = true;
            int id = 0;
            do
            {
                try
                {
                    string number = Console.ReadLine();
                    id = IsValidInt(number);
                    if (id > 0 &&
                        id <= this.OrderLogic.CustomerRepo.GetAll().ToList().Count &&
                        this.OrderLogic.CustomerRepo.GetOne(id) != null)
                    {
                        badNumber = false;
                    }
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }

                if (badNumber)
                {
                    Console.WriteLine("Invalid number at validation");
                }
            }
            while (badNumber);
            return id;

            // This is not the best method for validation,
            // but get's the job done ( not memory-efficient)
        }

        /// <summary>
        /// Validate the id from the console.
        /// </summary>
        /// <returns>The id as an integer.</returns>
        private int ProductIDValidation()
        {
            Console.WriteLine();
            Console.WriteLine("Write the ID: ");
            bool badNumber = true;
            int id = 0;
            do
            {
                try
                {
                    string number = Console.ReadLine();
                    id = IsValidInt(number);
                    if (id > 0 &&
                        id <= this.OrderLogic.ProductRepo.GetAll().ToList().Count &&
                        this.OrderLogic.ProductRepo.GetOne(id) != null)
                    {
                        badNumber = false;
                    }
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }

                if (badNumber)
                {
                    Console.WriteLine("Invalid number");
                }
            }
            while (badNumber);
            return id;
        }

        /// <summary>
        /// Validate the id from the console.
        /// </summary>
        /// <returns>The id as an integer.</returns>
        private int ManufacturerIDValidation()
        {
            Console.WriteLine();
            Console.WriteLine("Write the ID: ");
            bool badNumber = true;
            int id = 0;
            do
            {
                try
                {
                    string number = Console.ReadLine();
                    id = IsValidInt(number);
                    if (id > 0 &&
                        id <= this.AdminLogic.ManufacturerRepo.GetAll().ToList().Count &&
                        this.AdminLogic.ManufacturerRepo.GetOne(id) != null)
                    {
                        badNumber = false;
                    }
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }

                if (badNumber)
                {
                    Console.WriteLine("Invalid number");
                }
            }
            while (badNumber);
            return id;
        }

        /// <summary>
        /// Validate the id from the console.
        /// </summary>
        /// <returns>The id as an integer.</returns>
        private int OrderIDValidation()
        {
            Console.WriteLine();
            Console.WriteLine("Write the ID: ");
            bool badNumber = true;
            int id = 0;
            do
            {
                try
                {
                    string number = Console.ReadLine();
                    id = IsValidInt(number);
                    if (id > 0 &&
                        id <= this.OrderLogic.OrderRepo.GetAll().ToList().Count &&
                        this.OrderLogic.OrderRepo.GetOne(id) != null)
                    {
                        badNumber = false;
                    }
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }

                if (badNumber)
                {
                    Console.WriteLine("Invalid number");
                }
            }
            while (badNumber);
            return id;
        }

        // In hungarian: Megpróbáltam az id validation-t jobban megoldani, de mégtöbb problémám lett, így maradt ez
    }
}
