﻿// -----------------------------------------------------------------------
// <copyright file="MenuUIChangeMethods.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MedicSupplier.Program
{
    using System;
    using System.Net.Mail;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Represents the class responsible for the user interface.
    /// </summary>
    public partial class MenuUI
    {
        /// <summary>
        /// Changes the customer's name using the console menu.
        /// </summary>
        public void ChangeCustomerName()
        {
            int id = this.CustomerIDValidation();

            Console.WriteLine("Change customer name, write the new name: ");
            bool badName = true;
            string newName;
            do
            {
                newName = Console.ReadLine();

                // Checks for length, and letters only with regex
                if (newName.Length < 50 && Regex.IsMatch(newName, @"^[a-zA-Z]+$"))
                {
                    badName = false;
                }
                else
                {
                    Console.WriteLine("Invalid name, please try again");
                }
            }
            while (badName);
            this.AdminLogic.UpdateCustomerName(id, newName);
            Console.WriteLine();
            Console.WriteLine("The customer with the new name:");
            Console.WriteLine(this.Factory.AdminLogic.GetOneCustomer(id).ToString());
            Console.ReadKey();
        }

        /// <summary>
        /// Changes the customer's address using the console menu.
        /// </summary>
        public void ChangeCustomerAddress()
        {
            int id = this.CustomerIDValidation();

            Console.WriteLine("Change customer address, write the new address: ");
            bool badAddress = true;
            string newAddress;
            do
            {
                newAddress = Console.ReadLine();

                // Checks for length, and only letters and number with regex
                if (newAddress.Length < 50 && Regex.IsMatch(newAddress, @"^[a-zA-Z0-9]+$"))
                {
                    badAddress = false;
                }
            }
            while (badAddress);
            this.AdminLogic.UpdateCustomerAddress(id, newAddress);
            Console.WriteLine();
            Console.WriteLine("The customer with the new address:");
            Console.WriteLine(this.Factory.AdminLogic.GetOneCustomer(id).ToString());
            Console.ReadKey();
        }

        /// <summary>
        /// Changes the customer's country using the console menu.
        /// </summary>
        public void ChangeCustomerCountry()
        {
            int id = this.CustomerIDValidation();

            Console.WriteLine("Change customer country, write the new country: ");
            bool badCountry = true;
            string newCountry;
            do
            {
                newCountry = Console.ReadLine();

                // Checks for length and only letter with regex
                if (newCountry.Length < 50 && Regex.IsMatch(newCountry, @"^[a-zA-Z]+$"))
                {
                    badCountry = false;
                }
            }
            while (badCountry);
            this.AdminLogic.UpdateCustomerCountry(id, newCountry);
            Console.WriteLine();
            Console.WriteLine("The customer with the new country:");
            Console.WriteLine(this.Factory.AdminLogic.GetOneCustomer(id).ToString());
            Console.ReadKey();
        }

        /// <summary>
        /// Changes the customer's phone number using the console menu.
        /// Checks for length 13 (Austria).
        /// </summary>
        public void ChangeCustomerPhone()
        {
            int id = this.CustomerIDValidation();

            Console.WriteLine("Change customer phone number, write the new phone number: ");
            bool badPhone = true;
            string newPhone;
            do
            {
                newPhone = Console.ReadLine();
                if (IsPhoneNumber(newPhone))
                {
                    badPhone = false;
                    Console.WriteLine("Correctly entered");
                }
                else
                {
                    Console.WriteLine("Incorrectly entered");
                }
            }
            while (badPhone);
            this.AdminLogic.UpdateCustomerPhone(id, newPhone);
            Console.WriteLine();
            Console.WriteLine("The customer with the new phone number:");
            Console.WriteLine(this.Factory.AdminLogic.GetOneCustomer(id).ToString());
            Console.ReadKey();
        }

        // ez nem biztos hogy kell, mint feature
        // public void ChangeCustomerLastOrderDate()
        // {
        //    Console.WriteLine();
        //    Console.WriteLine("Change customer name, write the ID: ");
        //    int id = int.Parse(Console.ReadLine(), Info);
        //    Console.WriteLine("Change customer name, write the new name: ");
        //    string newName = Console.ReadLine();
        //    this.AdminLogic.UpdateCustomerName(id, newName);
        //    Console.WriteLine();
        //    Console.WriteLine("The customer with the new name:");
        //    Console.WriteLine(this.Factory.AdminLogic.GetOneCustomer(id).ToString());
        //    Console.ReadKey();
        // }

        /// <summary>
        /// Changes the customer's email using the console menu.
        /// </summary>
        public void ChangeCustomerEmail()
        {
            int id = this.CustomerIDValidation();

            Console.WriteLine("Change customer email, write the new email: ");
            bool badEmail = true;
            string newEmail;
            do
            {
                newEmail = Console.ReadLine();
                try
                {
                    string email = new MailAddress(newEmail).Address;
                    if (email.Length < 50)
                    {
                        badEmail = false;
                    }
                    else
                    {
                        Console.WriteLine("Too long(50+) email address");
                    }
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            while (badEmail);
            this.AdminLogic.UpdateCustomerEmail(id, newEmail);
            Console.WriteLine();
            Console.WriteLine("The customer with the new email:");
            Console.WriteLine(this.Factory.AdminLogic.GetOneCustomer(id).ToString());
            Console.ReadKey();
        }

        /// <summary>
        /// Changes the manufacturer's name using the console menu.
        /// </summary>
        public void ChangeManufacturerName()
        {
            int id = this.ManufacturerIDValidation();
            Console.WriteLine("Write the new name: ");
            bool badName = true;
            string newName;
            do
            {
                newName = Console.ReadLine();

                // Checks for length, and only letters with regex
                if (newName.Length < 50 && Regex.IsMatch(newName, @"^[a-zA-Z]+$"))
                {
                    badName = false;
                }
            }
            while (badName);
            this.AdminLogic.UpdateManufacturerName(id, newName);
            Console.WriteLine();
            Console.WriteLine("The manufacturer with the new name:");
            Console.WriteLine(this.Factory.AdminLogic.GetOneManufacturer(id).ToString());
            Console.ReadKey();
        }

        /// <summary>
        /// Changes the manufacturer's address using the console menu.
        /// </summary>
        public void ChangeManufacturerAddress()
        {
            int id = this.ManufacturerIDValidation();
            Console.WriteLine("Write the new address: ");
            bool badAddress = true;
            string newAddress;
            do
            {
                newAddress = Console.ReadLine();

                // Checks for length, and only letters and number with regex
                if (newAddress.Length < 50 && Regex.IsMatch(newAddress, @"^[a-zA-Z0-9]+$"))
                {
                    badAddress = false;
                }
            }
            while (badAddress);
            this.AdminLogic.UpdateManufacturerAddress(id, newAddress);
            Console.WriteLine();
            Console.WriteLine("The manufacturer with the new address:");
            Console.WriteLine(this.Factory.AdminLogic.GetOneManufacturer(id).ToString());
            Console.ReadKey();
        }

        /// <summary>
        /// Changes the manufacturer's country using the console menu.
        /// </summary>
        public void ChangeManufacturerCountry()
        {
            int id = this.ManufacturerIDValidation();
            Console.WriteLine("Write the new country: ");
            bool badCountry = true;
            string newCountry;
            do
            {
                newCountry = Console.ReadLine();

                // Checks for length and only letter with regex
                if (newCountry.Length < 50 && Regex.IsMatch(newCountry, @"^[a-zA-Z]+$"))
                {
                    badCountry = false;
                }
            }
            while (badCountry);
            this.AdminLogic.UpdateManufacturerCountry(id, newCountry);
            Console.WriteLine();
            Console.WriteLine("The manufacturer with the new country:");
            Console.WriteLine(this.Factory.AdminLogic.GetOneManufacturer(id).ToString());
            Console.ReadKey();
        }

        /// <summary>
        /// Changes the manufacturer's phone number using the console menu.
        /// </summary>
        public void ChangeManufacturerPhone()
        {
            int id = this.ManufacturerIDValidation();
            Console.WriteLine("Write the new phone number: ");
            bool badPhone = true;
            string newPhone;
            do
            {
                newPhone = Console.ReadLine();
                if (IsPhoneNumber(newPhone))
                {
                    badPhone = false;
                    Console.WriteLine("Correctly entered");
                }
                else
                {
                    Console.WriteLine("Incorrectly entered");
                }
            }
            while (badPhone);
            this.AdminLogic.UpdateManufacturerPhone(id, newPhone);
            Console.WriteLine();
            Console.WriteLine("The manufacturer with the new phone number:");
            Console.WriteLine(this.Factory.AdminLogic.GetOneManufacturer(id).ToString());
            Console.ReadKey();
        }

        /// <summary>
        /// Changes the manufacturer's revenue using the console menu.
        /// </summary>
        public void ChangeManufacturerRevenue()
        {
            int id = this.ManufacturerIDValidation();
            Console.WriteLine("Write the new revenue: ");
            bool badNumber = true;
            double newRevenue = 0;
            do
            {
                try
                {
                    string number = Console.ReadLine();
                    newRevenue = IsValidDouble(number);
                    if (newRevenue > 0)
                    {
                        badNumber = false;
                    }
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }

                if (badNumber)
                {
                    Console.WriteLine("Invalid number");
                }

                badNumber = false;
            }
            while (badNumber);
            this.AdminLogic.UpdateManufacturerRevenue(id, newRevenue);
            Console.WriteLine();
            Console.WriteLine("The manufacturer with the new revenue:");
            Console.WriteLine(this.Factory.AdminLogic.GetOneManufacturer(id).ToString());
            Console.ReadKey();
        }

        /// <summary>
        /// Changes the manufacturer's email using the console menu.
        /// </summary>
        public void ChangeManufacturerEmail()
        {
            int id = this.ManufacturerIDValidation();
            Console.WriteLine("Write the new email address: ");
            bool badEmail = true;
            string newEmail;
            do
            {
                newEmail = Console.ReadLine();
                try
                {
                    string email = new MailAddress(newEmail).Address;
                    if (email.Length < 50)
                    {
                        badEmail = false;
                    }
                    else
                    {
                        Console.WriteLine("Too long(50+) email address");
                    }
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            while (badEmail);
            this.AdminLogic.UpdateManufacturerEmail(id, newEmail);
            Console.WriteLine();
            Console.WriteLine("The manufacturer with the new email:");
            Console.WriteLine(this.Factory.AdminLogic.GetOneManufacturer(id).ToString());
            Console.ReadKey();
        }

        /// <summary>
        /// Changes the product's name using the console menu.
        /// </summary>
        public void ChangeProductName()
        {
            int id = this.ProductIDValidation();
            Console.WriteLine("Write the new name:");
            bool badName = true;
            string newName;
            do
            {
                newName = Console.ReadLine();

                // Checks for length, and only letters and number with regex
                if (newName.Length < 50 && Regex.IsMatch(newName, @"^[a-zA-Z]+$"))
                {
                    badName = false;
                }
            }
            while (badName);
            this.AdminLogic.UpdateProductName(id, newName);
            Console.WriteLine();
            Console.WriteLine("The product with the new name:");
            Console.WriteLine(this.Factory.OrderLogic.GetOneProduct(id).ToString());
            Console.ReadKey();
        }

        /// <summary>
        /// Changes the product's price using the console menu.
        /// </summary>
        public void ChangeProductPrice()
        {
            int id = this.ProductIDValidation();
            Console.WriteLine("Write the new price: ");
            bool badNumber = true;
            double newPrice = 0;
            do
            {
                try
                {
                    string number = Console.ReadLine();
                    newPrice = IsValidDouble(number);
                    if (newPrice > 0)
                    {
                        badNumber = false;
                    }
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }

                if (badNumber)
                {
                    Console.WriteLine("Invalid number");
                }
            }
            while (badNumber);
            this.OrderLogic.UpdateProductPrice(id, newPrice);
            Console.WriteLine();
            Console.WriteLine("The product with the new revenue:");
            Console.WriteLine(this.Factory.OrderLogic.GetOneProduct(id).ToString());
            Console.ReadKey();
        }

        /// <summary>
        /// Changes the product's quantity in stock using the console menu.
        /// </summary>
        public void ChangeProductInStock()
        {
            int id = this.ProductIDValidation();
            Console.WriteLine("Change product quantity in stock, write the new quantity: ");

            bool badNumber = true;
            int newQuantity = 0;
            do
            {
                try
                {
                    string number = Console.ReadLine();
                    newQuantity = IsValidInt(number);
                    if (newQuantity > 0)
                    {
                        badNumber = false;
                    }
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }

                if (badNumber)
                {
                    Console.WriteLine("Invalid number");
                }

                badNumber = false;
            }
            while (badNumber);
            this.OrderLogic.UpdateProductQuantity(id, newQuantity);
            Console.WriteLine();
            Console.WriteLine("The product with the new quantity:");
            Console.WriteLine(this.Factory.OrderLogic.GetOneProduct(id).ToString());
            Console.ReadKey();
        }

        /// <summary>
        /// Changes the product's reorder level using the console menu.
        /// </summary>
        public void ChangeProductReOrder()
        {
            int id = this.ProductIDValidation();
            Console.WriteLine("Change product reorder level, write the new reorder level: ");

            bool badNumber = true;
            int newReorder = 0;
            do
            {
                try
                {
                    string number = Console.ReadLine();
                    newReorder = IsValidInt(number);
                    if (newReorder > 0)
                    {
                        badNumber = false;
                    }
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }

                if (badNumber)
                {
                    Console.WriteLine("Invalid number");
                }

                badNumber = false;
            }
            while (badNumber);
            this.OrderLogic.UpdateProductOrderLevel(id, newReorder);
            Console.WriteLine();
            Console.WriteLine("The product with the reorder level:");
            Console.WriteLine(this.Factory.OrderLogic.GetOneProduct(id).ToString());
            Console.ReadKey();
        }

        /// <summary>
        /// Changes the product's classificiation using the console menu.
        /// </summary>
        public void ChangeProductClassif()
        {
            int id = this.ProductIDValidation();
            Console.WriteLine("Write the new classification:");
            bool badClassif = true;
            string newClassif;
            do
            {
                newClassif = Console.ReadLine();

                // Checks for length, and only number with regex
                if (newClassif.Length < 3 && Regex.IsMatch(newClassif, @"^[0-9]+$"))
                {
                    badClassif = false;
                }
            }
            while (badClassif);
            this.OrderLogic.UpdateProductClassification(id, newClassif);
            Console.WriteLine();
            Console.WriteLine("The product with the new classification:");
            Console.WriteLine(this.Factory.OrderLogic.GetOneProduct(id).ToString());
            Console.ReadKey();
        }

        /// <summary>
        /// Changes the order's quantity using the console menu.
        /// </summary>
        public void ChangeOrderQuantity()
        {
            int id = this.OrderIDValidation();
            Console.WriteLine("Write the new quantity:");
            bool badNumber = true;
            int newQuantity = 0;
            do
            {
                try
                {
                    string number = Console.ReadLine();
                    newQuantity = IsValidInt(number);
                    if (newQuantity > 0)
                    {
                        badNumber = false;
                    }
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }

                if (badNumber)
                {
                    Console.WriteLine("Invalid number");
                }

                badNumber = false;
            }
            while (badNumber);
            this.OrderLogic.UpdateOrderQuantity(id, newQuantity);
            Console.WriteLine();
            Console.WriteLine("The order with the new quantity:");
            Console.WriteLine(this.Factory.OrderLogic.GetOneOrder(id).ToString());
            Console.ReadKey();
        }

        /// <summary>
        /// Changes the order's deadline using the console menu.
        /// </summary>
        public void ChangeOrderDeadline()
        {
            int id = this.OrderIDValidation();
            bool badDate = true;
            DateTime newDeadline = DateTime.MinValue;
            Console.WriteLine("Write the new deadline:");
            do
            {
                string date = Console.ReadLine();
                if (DateTime.TryParse(date, out DateTime result))
                {
                    newDeadline = result;
                    badDate = false;
                }
                else
                {
                    Console.WriteLine("Incorrect deadline! Try again!");
                }
            }
            while (badDate);
            this.OrderLogic.UpdateOrderDeadline(id, newDeadline);
            Console.WriteLine();
            Console.WriteLine("The order with the new deadline:");
            Console.WriteLine(this.Factory.OrderLogic.GetOneOrder(id).ToString());
            Console.ReadKey();
        }
    }
}
