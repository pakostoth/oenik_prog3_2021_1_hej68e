﻿// -----------------------------------------------------------------------
// <copyright file="Factory.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MedicSupplier.Program
{
    using System;
    using MedicSupplier.Data.Models;
    using MedicSupplier.Logic;
    using MedicSupplier.Repository;

    /// <summary>
    /// Represents a factory class.
    /// </summary>
    public sealed class Factory : IDisposable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Factory"/> class.
        /// </summary>
        public Factory()
        {
            this.MedicSupplier = new MedicSupplierContext();
            this.CustomerRepository = new CustomerRepository(this.MedicSupplier);
            this.ManufacturerRepository = new ManufacturerRepository(this.MedicSupplier);
            this.OrderRepository = new OrderRepository(this.MedicSupplier);
            this.ProductRepository = new ProductRepository(this.MedicSupplier);
            this.AdminLogic = new AdminLogic(this.CustomerRepository, this.ProductRepository, this.ManufacturerRepository);
            this.OrderLogic = new OrderLogic(this.CustomerRepository, this.ProductRepository, this.OrderRepository);
            this.Menu = new MenuUI(this.AdminLogic, this.OrderLogic, this);
            this.Menu.Settings();
        }

        /// <summary>
        /// Gets the database context class.
        /// </summary>
        public MedicSupplierContext MedicSupplier { get; private set; }

        /// <summary>
        /// Gets the Data layer for the customer table.
        /// </summary>
        public CustomerRepository CustomerRepository { get; private set; }

        /// <summary>
        /// Gets the Data layer for the manufacturer table.
        /// </summary>
        public ManufacturerRepository ManufacturerRepository { get; private set; }

        /// <summary>
        /// Gets the Data layer for the order table.
        /// </summary>
        public OrderRepository OrderRepository { get; private set; }

        /// <summary>
        /// Gets the Data layer for the product table.
        /// </summary>
        public ProductRepository ProductRepository { get; private set; }

        /// <summary>
        /// Gets the Administrative controller.
        /// </summary>
        public AdminLogic AdminLogic { get; private set; }

        /// <summary>
        /// Gets the Order-related controller.
        /// </summary>
        public OrderLogic OrderLogic { get; private set; }

        /// <summary>
        /// Gets the menu for controlling the console.
        /// </summary>
        public MenuUI Menu { get; private set; }

        /// <summary>
        /// Disposes the factory class.
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
