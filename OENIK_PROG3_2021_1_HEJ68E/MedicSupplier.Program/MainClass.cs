﻿// -----------------------------------------------------------------------
// <copyright file="MainClass.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
using System;

[assembly: CLSCompliant(false)]

namespace MedicSupplier.Program
{
    /// <summary>
    /// The main program class.
    /// </summary>
    public static class MainClass
    {
        /// <summary>
        /// The main method.
        /// </summary>
        public static void Main()
        {
            Factory f = new Factory();

            f.Dispose();
        }
    }
}
