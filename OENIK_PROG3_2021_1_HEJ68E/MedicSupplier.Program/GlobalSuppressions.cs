﻿// -----------------------------------------------------------------------
// <copyright file="GlobalSuppressions.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "There is no need to use resource file for 2-3 line string in the same language")]
[assembly: SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1123:Do not place regions within elements", Justification = "Region helps to maintain a clean, readable code. Only used in MenuUI.cs")]
