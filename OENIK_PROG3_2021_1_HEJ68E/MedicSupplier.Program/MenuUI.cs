﻿// -----------------------------------------------------------------------
// <copyright file="MenuUI.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MedicSupplier.Program
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading;
    using ConsoleTools;
    using MedicSupplier.Logic;

    /// <summary>
    /// Represents the class responsible for the user interface.
    /// </summary>
    public partial class MenuUI
    {
        private static readonly CultureInfo Info = new CultureInfo("hu-HU");

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuUI"/> class.
        /// </summary>
        /// <param name="adminLogic">The admin logic controller.</param>
        /// <param name="orderLogic">The order logic controller.</param>
        /// <param name="factory">The factory object.</param>
        public MenuUI(AdminLogic adminLogic, OrderLogic orderLogic, Factory factory)
        {
            this.AdminLogic = adminLogic;
            this.OrderLogic = orderLogic;
            this.Factory = factory;
            this.Menu = new ConsoleMenu();
        }

        /// <summary>
        /// Gets the admin logic.
        /// </summary>
        public AdminLogic AdminLogic { get; }

        /// <summary>
        /// Gets the order logic.
        /// </summary>
        public OrderLogic OrderLogic { get; }

        /// <summary>
        /// Gets the console menu.
        /// </summary>
        public ConsoleMenu Menu { get; private set; }

        /// <summary>
        /// Gets the factory object.
        /// </summary>
        public Factory Factory { get; }

        /// <summary>
        /// Sets up the menu, with the submenus as well.
        /// </summary>
        public void Settings()
        {
            #region SubMenu definition

            // Setting up the menu for listing one entity.
            var listOneMenu = new ConsoleMenu()
                .Add("List one customer", () => this.ListOneCustomer())
                .Add("List one manufacturer", () => this.ListOneManufacturer())
                .Add("List one product", () => this.ListOneProduct())
                .Add("List one order", () => this.ListOneOrder())
                .Add("Back to the main menu", ConsoleMenu.Close)
                .Configure(config =>
                {
                    config.Selector = "--> ";
                    config.Title = "List One Menu";
                });

            // Setting up the menu for listing all entities.
            var listAllMenu = new ConsoleMenu()
                .Add("List all customers", () => this.ListAllCustomer())
                .Add("List all manufacturers", () => this.ListAllManufacturer())
                .Add("List all products", () => this.ListAllProduct())
                .Add("List all orders", () => this.ListAllOrder())
                .Add("Back to the main menu", ConsoleMenu.Close)
                .Configure(config =>
                {
                    config.Selector = "--> ";
                    config.Title = "List All Menu";
                });

            // Setting up the menu for adding new entity.
            var addMenu = new ConsoleMenu()
                .Add("Add a new customer", () => this.AddCustomer())
                .Add("Add a new manufacturer", () => this.AddManufacturer())
                .Add("Add a new product", () => this.AddProduct())
                .Add("Add a new order", () => this.AddOrder())
                .Add("Back to the main menu", ConsoleMenu.Close)
                .Configure(config =>
                {
                    config.Selector = "--> ";
                    config.Title = "Add Menu";
                });

            // Setting up the menu for removing an entity.
            var deleteMenu = new ConsoleMenu()
                .Add("Remove a customer", () => this.DeleteCustomer())
                .Add("Remove a manufacturer", () => this.DeleteManufacturer())
                .Add("Remove a product", () => this.DeleteProduct())
                .Add("Removed an order", () => this.DeleteOrder())
                .Add("Back to the main menu", ConsoleMenu.Close)
                .Configure(config =>
                {
                    config.Selector = "--> ";
                    config.Title = "Delete Menu";
                });

            // Setting up the menu for updating a customer.
            var updateCustomerMenu = new ConsoleMenu()
                .Add("Update a customer's name", () => this.ChangeCustomerName())
                .Add("Update a customer's address", () => this.ChangeCustomerAddress())
                .Add("Update a customer's country", () => this.ChangeCustomerCountry())
                .Add("Update a customer's phone number", () => this.ChangeCustomerPhone())
                .Add("Update a customer's email", () => this.ChangeCustomerEmail())
                .Add("Back to the main menu", ConsoleMenu.Close)
                .Configure(config =>
                {
                    config.Selector = "--> ";
                    config.Title = "Update Customer Menu";
                });

            // Setting up the menu for updating a manufacturer.
            var updateManufacturerMenu = new ConsoleMenu()
                .Add("Update a manufacturer's name", () => this.ChangeManufacturerName())
                .Add("Update a manufacturer's address", () => this.ChangeManufacturerAddress())
                .Add("Update a manufacturer's country", () => this.ChangeManufacturerCountry())
                .Add("Update a manufacturer's phone number", () => this.ChangeManufacturerPhone())
                .Add("Update a manufacturer's revenue", () => this.ChangeManufacturerRevenue())
                .Add("Update a manufacturer's email", () => this.ChangeManufacturerEmail())
                .Add("Back to the main menu", ConsoleMenu.Close)
                .Configure(config =>
                {
                    config.Selector = "--> ";
                    config.Title = "Update Manufacturer Menu";
                });

            // Setting up the menu for updating a product.
            var updateProductMenu = new ConsoleMenu()
                .Add("Update a product's name", () => this.ChangeProductName())
                .Add("Update a product's price", () => this.ChangeProductPrice())
                .Add("Update a product's quantity in stock", () => this.ChangeProductInStock())
                .Add("Update a product's reorder", () => this.ChangeProductReOrder())
                .Add("Update a product's classification", () => this.ChangeProductClassif())
                .Add("Back to the main menu", ConsoleMenu.Close)
                .Configure(config =>
                {
                    config.Selector = "--> ";
                    config.Title = "Update Product Menu";
                });

            // Setting up the menu for updating an order.
            var updateOrderMenu = new ConsoleMenu()
                .Add("Update an order's quantity", () => this.ChangeOrderQuantity())
                .Add("Update an order's deadline", () => this.ChangeOrderDeadline())
                .Add("Back to the main menu", ConsoleMenu.Close)
                .Configure(config =>
                {
                    config.Selector = "--> ";
                    config.Title = "Update Order Menu";
                });

            // Setting up the menu for updating an entity.
            var updateEntityMenu = new ConsoleMenu()
                .Add("Update a customer", updateCustomerMenu.Show)
                .Add("Update a manufacturer", updateManufacturerMenu.Show)
                .Add("Update a product", updateProductMenu.Show)
                .Add("Update a order", updateOrderMenu.Show)
                .Add("Back to the main menu", ConsoleMenu.Close)
                .Configure(config =>
                {
                    config.Selector = "--> ";
                    config.Title = "Update entity Menu";
                });

            var nonCrudMenu = new ConsoleMenu()
                .Add("List customers with orders", () => this.ListCustomersWithOrders())
                .Add("(Async version)", () => this.ListCustomersWithOrdersAsync())
                .Add("List product quantity by classification", () => this.ListProductbyClassification())
                .Add("(Async version)", () => this.ListProductbyClassificationAsync())
                .Add("List products by country", () => this.ListProductbyCountry())
                .Add("(Async version)", () => this.ListProductbyCountryAsync())
                .Add("Back to the main menu", ConsoleMenu.Close)
                .Configure(config =>
                {
                    config.Selector = "--> ";
                    config.Title = "NonCrud Menu";
                });
            #endregion

            // Setting up the main menu.
            this.Menu
                .Add("List one entity", listOneMenu.Show)
                .Add("List all entities", listAllMenu.Show)
                .Add("Add new entities", addMenu.Show)
                .Add("Remove an entity", deleteMenu.Show)
                .Add("Update an entity", updateEntityMenu.Show)
                .Add("Complex queries", nonCrudMenu.Show)
                .Add("Exit", ConsoleMenu.Close)
                .Configure(config =>
                {
                    config.Selector = "--> ";
                    config.Title = "Main Menu";
                })
                .Show();
        }

        // Extension methods. Used only in console operation.

        /// <summary>
        /// Write to the console at the end of the query.
        /// </summary>
        private static void QueryComplete()
        {
            Console.WriteLine();
            Console.WriteLine("Query completed");
            Thread.Sleep(500);
            Console.WriteLine();
            Console.WriteLine("Press enter to continue");
            Console.ReadKey();
        }

        // If a place here do-while cycles then the code would be more readable

        /// <summary>
        /// Checks if the string is a valid number.
        /// </summary>
        /// <param name="number">The number to validate.</param>
        /// <returns>The number as an integer.</returns>
        private static int IsValidInt(string number)
        {
            try
            {
                if (int.TryParse(number, out int result))
                {
                    return int.Parse(number, Info);
                }
                else
                {
                    throw new ArgumentException(message: "Invalid number, try again!");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Checks if the string is a valid number.
        /// </summary>
        /// <param name="number">The number to validate.</param>
        /// <returns>The number as a double.</returns>
        private static double IsValidDouble(string number)
        {
            try
            {
                if (double.TryParse(number, out double result))
                {
                    return double.Parse(number, Info);
                }
                else
                {
                    throw new ArgumentException(message: "Invalid number, try again!");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Checks if the string parameter is a valid european phone number using regex.
        /// </summary>
        /// <param name="number">The string to check.</param>
        /// <returns>True if it is a phone number.</returns>
        private static bool IsPhoneNumber(string number)
        {
            if (number.Length > 11 && number.Length < 4)
            {
                return false;
            }
            else
            {
                foreach (char c in number)
                {
                    if (c < '0' || c > '9')
                    {
                        return false;
                    }
                }

                return true;
            }
        }
    }
}
