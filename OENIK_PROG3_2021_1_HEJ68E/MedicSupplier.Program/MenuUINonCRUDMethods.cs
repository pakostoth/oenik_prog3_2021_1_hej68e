﻿// -----------------------------------------------------------------------
// <copyright file="MenuUINonCRUDMethods.cs" company="MedicSupplier">
// "Copyright (c) MedicSupplier. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace MedicSupplier.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;

    /// <summary>
    /// Represents the class responsible for the user interface.
    /// </summary>
    public partial class MenuUI
    {
        /// <summary>
        /// Lists products by country to the console.
        /// </summary>
        public void ListProductbyCountry()
        {
            Console.Clear();
            Console.WriteLine("The manufacturers' countries:");
            List<string> fixCountries = this.AdminLogic.ListAllCountry().ToList();
            foreach (var fixCountry in fixCountries)
            {
                Console.WriteLine(fixCountry);
            }

            Console.WriteLine("List product by country, write the country: ");
            Console.WriteLine();
            Console.WriteLine("Write 'back' to step back to the menu");
            string country;
            bool getOut = true;
            do
            {
                country = Console.ReadLine();
                if (country.ToLower(Info) == "back")
                {
                    return;
                }

                if (fixCountries.Any(fixCountry => fixCountry.ToLower(Info) == country.ToLower(Info)))
                {
                    getOut = false;
                }
                else
                {
                    Console.WriteLine("The country you typed is not valid, try again:");
                }
            }
            while (getOut);

            Console.Clear();

            var result = this.AdminLogic.ListProductsByCountry(country).ToList();

            Console.WriteLine("Products from {0}", country);
            Console.WriteLine();
            System.Console.WriteLine(string.Format(Info, "{0,-5}{1,-30}{2,-40}{3,-20}{4,-20}{5,-20}", "ID", "Manufacturer Name", "Product name", "Quantity in stock", "Reorder level", "Classification"));
            Console.WriteLine();
            foreach (var x in result)
            {
                System.Console.WriteLine(
                    string.Format(
                        Info,
                        "{0,-5}{1,-30}{2,-40}{3,-20}{4,-20}{5,-20}",
                        x.ProductID,
                        x.Manufacturer.Name,
                        x.Name,
                        x.InStock.ToString(Info),
                        x.ReorderLevel.ToString(Info),
                        x.Classification));
                Thread.Sleep(100);
            }

            QueryComplete();
        }

        /// <summary>
        /// Lists products by country to the console. Async version.
        /// </summary>
        public void ListProductbyCountryAsync()
        {
            Console.Clear();
            Console.WriteLine("The manufacturers' countries:");
            List<string> fixCountries = this.AdminLogic.ListAllCountry().ToList();
            foreach (var fixCountry in fixCountries)
            {
                Console.WriteLine(fixCountry);
            }

            Console.WriteLine("List product by country, write the country: ");
            Console.WriteLine();
            Console.WriteLine("Write 'back' to step back to the menu");
            string country;
            bool getOut = true;
            do
            {
                country = Console.ReadLine();
                if (country.ToLower(Info) == "back")
                {
                    return;
                }

                if (fixCountries.Any(fixCountry => fixCountry.ToLower(Info) == country.ToLower(Info)))
                {
                    getOut = false;
                }
                else
                {
                    Console.WriteLine("The country you typed is not valid, try again:");
                }
            }
            while (getOut);

            Console.WriteLine();

            var temp = this.AdminLogic.ListProductsByCountryAsync(country);

            Console.Clear();
            Console.WriteLine("Collecting data....");
            Thread.Sleep(1500);
            Console.Clear();

            Console.WriteLine("Products from {0}", country);
            Console.WriteLine();
            System.Console.WriteLine(string.Format(Info, "{0,-5}{1,-30}{2,-40}{3,-20}{4,-20}{5,-20}", "ID", "Manufacturer Name", "Product name", "Quantity in stock", "Reorder level", "Classification"));
            Console.WriteLine();
            foreach (var x in temp.Result)
            {
                System.Console.WriteLine(
                    string.Format(
                        Info,
                        "{0,-5}{1,-30}{2,-40}{3,-20}{4,-20}{5,-20}",
                        x.ProductID,
                        x.Manufacturer.Name,
                        x.Name,
                        x.InStock.ToString(Info),
                        x.ReorderLevel.ToString(Info),
                        x.Classification));
                Thread.Sleep(100);
            }

            Thread.Sleep(300);
            QueryComplete();
        }

        /// <summary>
        /// Lists customers with orders to the console.
        /// </summary>
        public void ListCustomersWithOrders()
        {
            Console.Clear();

            Console.WriteLine("The customers with orders:");
            Console.WriteLine();
            System.Console.WriteLine(string.Format(Info, "{0,-5}{1,-30}{2,-40}{3,-20}{4,-20}{5,-20}{6,-30}", "ID", "Name", "Address", "Country", "Phone number", "Last order date", "Email"));
            Console.WriteLine();

            var result = this.OrderLogic.CustomersWithOrder().ToList();

            foreach (var x in result)
            {
                System.Console.WriteLine(
                    string.Format(Info, "{0,-5}{1,-30}{2,-40}{3,-20}{4,-20}{5,-20}{6,-30}", x.CustomerID, x.Name, x.Address, x.Country, x.PhoneNumber, x.LastOrderDate.ToShortDateString(), x.Email));
                Thread.Sleep(100);
            }

            Thread.Sleep(300);
            QueryComplete();
        }

        /// <summary>
        /// Lists customers with orders to the console. Async version.
        /// </summary>
        public void ListCustomersWithOrdersAsync()
        {
            Console.Clear();
            Console.WriteLine("Collecting data....");
            Thread.Sleep(1500);
            Console.Clear();

            Console.WriteLine("The customers with orders:");
            Console.WriteLine();
            System.Console.WriteLine(string.Format(Info, "{0,-5}{1,-30}{2,-40}{3,-20}{4,-20}{5,-20}{6,-30}", "ID", "Name", "Address", "Country", "Phone number", "Last order date", "Email"));
            Console.WriteLine();

            var temp = this.OrderLogic.CustomersWithOrderAsync();

            foreach (var x in temp.Result)
            {
                System.Console.WriteLine(
                    string.Format(Info, "{0,-5}{1,-30}{2,-40}{3,-20}{4,-20}{5,-20}{6,-30}", x.CustomerID, x.Name, x.Address, x.Country, x.PhoneNumber, x.LastOrderDate.ToShortDateString(), x.Email));
                Thread.Sleep(100);
            }

            Thread.Sleep(300);
            QueryComplete();
        }

        /// <summary>
        /// Lists the orders by products by a specific classification.
        /// </summary>
        public void ListProductbyClassification()
        {
            Console.Clear();

            Console.WriteLine("The classfication values:");
            Console.WriteLine();
            List<string> fixclassifs = this.OrderLogic.ListAllClassification().ToList();
            foreach (var fixClassif in fixclassifs)
            {
                Console.WriteLine(fixClassif);
            }

            Console.WriteLine();
            Console.WriteLine("The product numbers by classfication, write the classfication: ");
            Console.WriteLine();
            Console.WriteLine("Write 'back' to step back to the menu");
            string classif;
            bool getOut = true;
            do
            {
                classif = Console.ReadLine();
                if (classif.ToLower(Info) == "back")
                {
                    return;
                }

                if (fixclassifs.Any(fixClassif => fixClassif.ToLower(Info) == classif.ToLower(Info)))
                {
                    getOut = false;
                }
                else
                {
                    Console.WriteLine("The classfication you typed is not valid, try again:");
                }
            }
            while (getOut);

            Console.Clear();
            System.Console.WriteLine(string.Format(Info, "{0,-20}{1,-10}{2,-20}", "Classification", "ProductID", "Number of orders"));

            var result = this.OrderLogic.ProductsAndOrdersbyClassification(classif).ToList();

            foreach (var x in result)
            {
                System.Console.WriteLine(
                    string.Format(Info, "{0,-20}{1,-10}{2,-15}", x.Classification, x.ProductID, x.OrderNumber));
                Thread.Sleep(100);
            }

            Thread.Sleep(300);
            QueryComplete();
        }

        /// <summary>
        /// Lists the orders by products by a specific classification. Async version.
        /// </summary>
        public void ListProductbyClassificationAsync()
        {
            Console.Clear();

            Console.WriteLine("The classfication values:");
            Console.WriteLine();
            List<string> fixclassifs = this.OrderLogic.ListAllClassification().ToList();
            foreach (var fixClassif in fixclassifs)
            {
                Console.WriteLine(fixClassif);
            }

            Console.WriteLine();
            Console.WriteLine("The product numbers by classfication, write the classfication: ");
            Console.WriteLine();
            Console.WriteLine("Write 'back' to step back to the menu");
            string classif;
            bool getOut = true;
            do
            {
                classif = Console.ReadLine();
                if (classif.ToLower(Info) == "back")
                {
                    return;
                }

                if (fixclassifs.Any(fixClassif => fixClassif.ToLower(Info) == classif.ToLower(Info)))
                {
                    getOut = false;
                }
                else
                {
                    Console.WriteLine("The classfication you typed is not valid, try again:");
                }
            }
            while (getOut);

            Console.Clear();
            Console.WriteLine("Collecting data....");
            Thread.Sleep(1500);
            Console.Clear();

            Console.WriteLine("The product numbers by classfication:");
            Console.WriteLine();
            System.Console.WriteLine(string.Format(Info, "{0,-20}{1,-10}{2,-20}", "Classification", "ProductID", "Number of orders"));

            var temp = this.OrderLogic.ProductsAndOrdersbyClassificationAsync(classif);

            foreach (var x in temp.Result)
            {
                System.Console.WriteLine(
                    string.Format(Info, "{0,-20}{1,-10}{2,-15}", x.Classification, x.ProductID, x.OrderNumber));
                Thread.Sleep(100);
            }

            Thread.Sleep(300);
            QueryComplete();
        }
    }
}
