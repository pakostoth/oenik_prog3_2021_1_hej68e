var class_medic_supplier_1_1_data_1_1_models_1_1_order =
[
    [ "Order", "class_medic_supplier_1_1_data_1_1_models_1_1_order.html#a494c990d9773c29097dc857ae6850b90", null ],
    [ "Order", "class_medic_supplier_1_1_data_1_1_models_1_1_order.html#ac900a5f5b1477f1183ae86f7e8000356", null ],
    [ "Order", "class_medic_supplier_1_1_data_1_1_models_1_1_order.html#a5cc72035e0a28c48fab9c5f0c1520de9", null ],
    [ "Equals", "class_medic_supplier_1_1_data_1_1_models_1_1_order.html#a985d764b6920a8218f733ba1bc73334f", null ],
    [ "GetHashCode", "class_medic_supplier_1_1_data_1_1_models_1_1_order.html#abd069f87cfe45d8bf1b3015815d9caa9", null ],
    [ "ToString", "class_medic_supplier_1_1_data_1_1_models_1_1_order.html#a9c8018c0b8f7104f4d861b4f9cd0ca6c", null ],
    [ "Customer", "class_medic_supplier_1_1_data_1_1_models_1_1_order.html#a44c0a98de285ff84ef1ff0ecf1699331", null ],
    [ "CustomerID", "class_medic_supplier_1_1_data_1_1_models_1_1_order.html#a152a4fc6fa4cfce4f52346bb93f7f8b8", null ],
    [ "Deadline", "class_medic_supplier_1_1_data_1_1_models_1_1_order.html#a7cbda19bf26eb92b1f6966cfc375273a", null ],
    [ "OrderID", "class_medic_supplier_1_1_data_1_1_models_1_1_order.html#a8c33d71a977c2e5accc8e786730e3a8b", null ],
    [ "Product", "class_medic_supplier_1_1_data_1_1_models_1_1_order.html#a645aa6015ee52555a3ec2d39fd7f8869", null ],
    [ "ProductID", "class_medic_supplier_1_1_data_1_1_models_1_1_order.html#a1180c2bea74a44ee70dd733ef3b0eee8", null ],
    [ "Quantity", "class_medic_supplier_1_1_data_1_1_models_1_1_order.html#a58a770df924b67992f5ae1334a004cdd", null ]
];