var class_medic_supplier_1_1_logic_1_1_order_logic =
[
    [ "OrderLogic", "class_medic_supplier_1_1_logic_1_1_order_logic.html#a9ceb648bbeff289babd7ffef455ce281", null ],
    [ "AddOrder", "class_medic_supplier_1_1_logic_1_1_order_logic.html#aa8b3156d9ecc5d391ff4f9c4a7801deb", null ],
    [ "CustomersWithOrder", "class_medic_supplier_1_1_logic_1_1_order_logic.html#ad300ad2024ad940395149e770fb54556", null ],
    [ "CustomersWithOrderAsync", "class_medic_supplier_1_1_logic_1_1_order_logic.html#a8a9462875845bc9422c7fdd5050d9137", null ],
    [ "DeleteOrder", "class_medic_supplier_1_1_logic_1_1_order_logic.html#a4df1e2f94136d32686b56bfd686595bc", null ],
    [ "GetAllOrder", "class_medic_supplier_1_1_logic_1_1_order_logic.html#a5f4f37a066e451062e5db61c1eaf19a9", null ],
    [ "GetAllProduct", "class_medic_supplier_1_1_logic_1_1_order_logic.html#aa62c68f218a07d863dbac65f62847a3b", null ],
    [ "GetOneOrder", "class_medic_supplier_1_1_logic_1_1_order_logic.html#a4ab1d03810471db4c32f6d64633ffd11", null ],
    [ "GetOneProduct", "class_medic_supplier_1_1_logic_1_1_order_logic.html#a0760848c5c293c630f08ba488870f5a6", null ],
    [ "ListAllClassification", "class_medic_supplier_1_1_logic_1_1_order_logic.html#a2bd5f8048a04a0581e128591c8499579", null ],
    [ "ProductsAndOrdersbyClassification", "class_medic_supplier_1_1_logic_1_1_order_logic.html#ab4f7e9f95c1ae9780246205985d5aaa4", null ],
    [ "ProductsAndOrdersbyClassificationAsync", "class_medic_supplier_1_1_logic_1_1_order_logic.html#a126285b80e04fea9c66042f8aa0046f8", null ],
    [ "UpdateLastOrderDate", "class_medic_supplier_1_1_logic_1_1_order_logic.html#a8388e230f2cee083729147b25d2a711c", null ],
    [ "UpdateOrderDeadline", "class_medic_supplier_1_1_logic_1_1_order_logic.html#ac57af03761089d148327e0efc50ab521", null ],
    [ "UpdateOrderQuantity", "class_medic_supplier_1_1_logic_1_1_order_logic.html#aeb58fe8fe04667435f55ef508165a428", null ],
    [ "UpdateProductClassification", "class_medic_supplier_1_1_logic_1_1_order_logic.html#a89d07cd1b06f037cea58fc797d4ada2f", null ],
    [ "UpdateProductOrderLevel", "class_medic_supplier_1_1_logic_1_1_order_logic.html#adfec429d1e6756e81144a14161392373", null ],
    [ "UpdateProductPrice", "class_medic_supplier_1_1_logic_1_1_order_logic.html#aee86342c80315147fcef11054c32af16", null ],
    [ "UpdateProductQuantity", "class_medic_supplier_1_1_logic_1_1_order_logic.html#a24844d255f6bcb58e43c60bfc0fb7ee5", null ],
    [ "CustomerRepo", "class_medic_supplier_1_1_logic_1_1_order_logic.html#a995d77ff29b6c2be6df2efb2e9b96166", null ],
    [ "OrderRepo", "class_medic_supplier_1_1_logic_1_1_order_logic.html#af3f678f44cf689b8590ac032f366b14b", null ],
    [ "ProductRepo", "class_medic_supplier_1_1_logic_1_1_order_logic.html#a3b3df3076d5e7784e2632994321dea65", null ]
];