var interface_medic_supplier_1_1_repository_1_1_i_customer_repository =
[
    [ "ChangeAddress", "interface_medic_supplier_1_1_repository_1_1_i_customer_repository.html#a0071fe35b7ab92074ae328c2d6d0f03b", null ],
    [ "ChangeCountry", "interface_medic_supplier_1_1_repository_1_1_i_customer_repository.html#a7b6156ecbfe39a1b0ccdba3aa2c92c26", null ],
    [ "ChangeEmail", "interface_medic_supplier_1_1_repository_1_1_i_customer_repository.html#afa5795d9cc24328e075ab09a9260b8df", null ],
    [ "ChangeLastOrder", "interface_medic_supplier_1_1_repository_1_1_i_customer_repository.html#aae83d4f6e0794feb8ec3ec498f66bca3", null ],
    [ "ChangeName", "interface_medic_supplier_1_1_repository_1_1_i_customer_repository.html#a14d8b9d158f50ba35a754f1ef7a0ee9c", null ],
    [ "ChangePhone", "interface_medic_supplier_1_1_repository_1_1_i_customer_repository.html#a07102fc693ba6959c4ff031a0bb4cf13", null ]
];