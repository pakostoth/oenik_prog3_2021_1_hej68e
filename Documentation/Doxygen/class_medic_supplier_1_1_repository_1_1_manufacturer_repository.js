var class_medic_supplier_1_1_repository_1_1_manufacturer_repository =
[
    [ "ManufacturerRepository", "class_medic_supplier_1_1_repository_1_1_manufacturer_repository.html#ab84fc7845fb837721c1dca15c46ed4b6", null ],
    [ "ChangeAddress", "class_medic_supplier_1_1_repository_1_1_manufacturer_repository.html#a376f596e873ebf8c7428d9d9c4ec5bb2", null ],
    [ "ChangeCountry", "class_medic_supplier_1_1_repository_1_1_manufacturer_repository.html#a822d8e44692eb4813040af7b64806263", null ],
    [ "ChangeEmail", "class_medic_supplier_1_1_repository_1_1_manufacturer_repository.html#adbdfecb9f7aff109954f3b4f0675dd12", null ],
    [ "ChangeName", "class_medic_supplier_1_1_repository_1_1_manufacturer_repository.html#a2f58a4831aa3adf9d71f1f46291cae93", null ],
    [ "ChangePhone", "class_medic_supplier_1_1_repository_1_1_manufacturer_repository.html#afc9455be0a1a8bdb10e176b2be7a0957", null ],
    [ "ChangeRevenue", "class_medic_supplier_1_1_repository_1_1_manufacturer_repository.html#ac1b9b885c0352e5cb9a35eab5de18bc9", null ],
    [ "GetOne", "class_medic_supplier_1_1_repository_1_1_manufacturer_repository.html#a9dbda6ea17069feb6f41bf12bbe4142a", null ]
];