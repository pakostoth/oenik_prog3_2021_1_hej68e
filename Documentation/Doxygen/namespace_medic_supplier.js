var namespace_medic_supplier =
[
    [ "Data", "namespace_medic_supplier_1_1_data.html", "namespace_medic_supplier_1_1_data" ],
    [ "Logic", "namespace_medic_supplier_1_1_logic.html", "namespace_medic_supplier_1_1_logic" ],
    [ "Program", "namespace_medic_supplier_1_1_program.html", "namespace_medic_supplier_1_1_program" ],
    [ "Repository", "namespace_medic_supplier_1_1_repository.html", "namespace_medic_supplier_1_1_repository" ],
    [ "Tests", "namespace_medic_supplier_1_1_tests.html", "namespace_medic_supplier_1_1_tests" ]
];