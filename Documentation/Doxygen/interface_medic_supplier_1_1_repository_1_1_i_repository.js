var interface_medic_supplier_1_1_repository_1_1_i_repository =
[
    [ "GetAll", "interface_medic_supplier_1_1_repository_1_1_i_repository.html#a601ed8a21e5beeefd4ce74c308d351e8", null ],
    [ "GetOne", "interface_medic_supplier_1_1_repository_1_1_i_repository.html#ab77e002c32dc96ad7e795408265d6a17", null ],
    [ "Insert", "interface_medic_supplier_1_1_repository_1_1_i_repository.html#a62aae2ae46a51f440d44f33b37b2e647", null ],
    [ "Remove", "interface_medic_supplier_1_1_repository_1_1_i_repository.html#a01d4cf49b04fd1ec5fe3f024611bc770", null ],
    [ "Remove", "interface_medic_supplier_1_1_repository_1_1_i_repository.html#ac1fdd14b5b3c0db594f7977343fb5d76", null ]
];