var class_medic_supplier_1_1_repository_1_1_customer_repository =
[
    [ "CustomerRepository", "class_medic_supplier_1_1_repository_1_1_customer_repository.html#a3fdc7ca75e7c703c365fffaaa2a9eb74", null ],
    [ "ChangeAddress", "class_medic_supplier_1_1_repository_1_1_customer_repository.html#aeaf4cd9c7301b708c63c268bdb9130a3", null ],
    [ "ChangeCountry", "class_medic_supplier_1_1_repository_1_1_customer_repository.html#a4ce8ac87de14689e2e6ce5e78770a9c2", null ],
    [ "ChangeEmail", "class_medic_supplier_1_1_repository_1_1_customer_repository.html#af021f9ae57696515e4cfe4082e92509b", null ],
    [ "ChangeLastOrder", "class_medic_supplier_1_1_repository_1_1_customer_repository.html#a16c664560d9016c99fb913349c01a643", null ],
    [ "ChangeName", "class_medic_supplier_1_1_repository_1_1_customer_repository.html#aef3eb006f2ed8b04008afd0c1d59fd9c", null ],
    [ "ChangePhone", "class_medic_supplier_1_1_repository_1_1_customer_repository.html#a84737ab31ee79c06d35e30c46a3856f1", null ],
    [ "GetOne", "class_medic_supplier_1_1_repository_1_1_customer_repository.html#aeed48cf2ba8cf9908295b850800c633e", null ],
    [ "Remove", "class_medic_supplier_1_1_repository_1_1_customer_repository.html#a87c503f7e36fa8af215a8b802bc0cef3", null ]
];