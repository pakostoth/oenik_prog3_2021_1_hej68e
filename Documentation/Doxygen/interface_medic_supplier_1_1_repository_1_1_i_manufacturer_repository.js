var interface_medic_supplier_1_1_repository_1_1_i_manufacturer_repository =
[
    [ "ChangeAddress", "interface_medic_supplier_1_1_repository_1_1_i_manufacturer_repository.html#aa851b465af568742b8ef5f812e4f59d0", null ],
    [ "ChangeCountry", "interface_medic_supplier_1_1_repository_1_1_i_manufacturer_repository.html#ad1108ef4b8960e48285731ae8fc8859f", null ],
    [ "ChangeEmail", "interface_medic_supplier_1_1_repository_1_1_i_manufacturer_repository.html#aea72600109b6689d0fcb761cb0ab8d0b", null ],
    [ "ChangeName", "interface_medic_supplier_1_1_repository_1_1_i_manufacturer_repository.html#af257d2e26625b77e5b40af7c24dc830e", null ],
    [ "ChangePhone", "interface_medic_supplier_1_1_repository_1_1_i_manufacturer_repository.html#a47f3c385347d4f87af9ddcaf721aa13e", null ],
    [ "ChangeRevenue", "interface_medic_supplier_1_1_repository_1_1_i_manufacturer_repository.html#a4c18bf6e3620add528db1c229868b727", null ]
];