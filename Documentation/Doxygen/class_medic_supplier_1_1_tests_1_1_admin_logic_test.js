var class_medic_supplier_1_1_tests_1_1_admin_logic_test =
[
    [ "Setup", "class_medic_supplier_1_1_tests_1_1_admin_logic_test.html#a787778f5d7e37ff154ed0d5400360cab", null ],
    [ "TestDeleteCustomer", "class_medic_supplier_1_1_tests_1_1_admin_logic_test.html#ae3beca42b8f45607f3106c54c0a6d891", null ],
    [ "TestGetAllCustomer", "class_medic_supplier_1_1_tests_1_1_admin_logic_test.html#a5a0197fd3f62877dc2e656e2edeca485", null ],
    [ "TestListProductsByCountry", "class_medic_supplier_1_1_tests_1_1_admin_logic_test.html#a60fcf66e7283709c06b85334e3966a87", null ],
    [ "TestUpdateManufacturerName", "class_medic_supplier_1_1_tests_1_1_admin_logic_test.html#afb241870c87005df996357244cd95dc8", null ],
    [ "TestUpdateProductName", "class_medic_supplier_1_1_tests_1_1_admin_logic_test.html#a461b8047bdbcb19beb08b71090eadc9f", null ],
    [ "ALogic", "class_medic_supplier_1_1_tests_1_1_admin_logic_test.html#a09ff425136f469cfc8f72f45ec1a5ebe", null ],
    [ "Customers", "class_medic_supplier_1_1_tests_1_1_admin_logic_test.html#adce74356b05aac6637afbc6d43abe901", null ],
    [ "Manufacturers", "class_medic_supplier_1_1_tests_1_1_admin_logic_test.html#a2aee669d35918e2e6a11cd06946b72b8", null ],
    [ "Products", "class_medic_supplier_1_1_tests_1_1_admin_logic_test.html#acc6826a831b7e05b75741407afcb9f88", null ]
];