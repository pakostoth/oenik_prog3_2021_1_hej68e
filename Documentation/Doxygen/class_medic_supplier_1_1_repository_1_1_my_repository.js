var class_medic_supplier_1_1_repository_1_1_my_repository =
[
    [ "MyRepository", "class_medic_supplier_1_1_repository_1_1_my_repository.html#acd49e58551518847a0aca0944969221b", null ],
    [ "GetAll", "class_medic_supplier_1_1_repository_1_1_my_repository.html#a6cb8b83ec070e9b0515a08c81e9d9e35", null ],
    [ "GetOne", "class_medic_supplier_1_1_repository_1_1_my_repository.html#a77ca25809e384bf0986e6a96db0c917c", null ],
    [ "Insert", "class_medic_supplier_1_1_repository_1_1_my_repository.html#a014609fd3e5b1000d0852b67d100e223", null ],
    [ "Remove", "class_medic_supplier_1_1_repository_1_1_my_repository.html#a3999fe3005ef6e46893a3d0040fef237", null ],
    [ "Remove", "class_medic_supplier_1_1_repository_1_1_my_repository.html#ae209ec4a7ebd6d1764febf876594d6af", null ],
    [ "Ctx", "class_medic_supplier_1_1_repository_1_1_my_repository.html#a38b5f49869b83ac0dde26824b9e2f5cc", null ]
];