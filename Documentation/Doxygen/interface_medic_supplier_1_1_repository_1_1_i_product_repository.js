var interface_medic_supplier_1_1_repository_1_1_i_product_repository =
[
    [ "ChangeClassification", "interface_medic_supplier_1_1_repository_1_1_i_product_repository.html#a047c147073f42fc8851d2e2badf58232", null ],
    [ "ChangeInstock", "interface_medic_supplier_1_1_repository_1_1_i_product_repository.html#af399f874855a687405c92cc0e8f27df2", null ],
    [ "ChangeName", "interface_medic_supplier_1_1_repository_1_1_i_product_repository.html#afdb1bc2f42f0f3d265c0d7a64bb7c6d1", null ],
    [ "ChangePrice", "interface_medic_supplier_1_1_repository_1_1_i_product_repository.html#aec095fe8b8afb337a68b02ac4f9fcae0", null ],
    [ "ChangeReorder", "interface_medic_supplier_1_1_repository_1_1_i_product_repository.html#ac40f2e37dc6d52191e2414744cc08812", null ]
];