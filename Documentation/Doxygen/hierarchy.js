var hierarchy =
[
    [ "MedicSupplier.Tests.AdminLogicTest", "class_medic_supplier_1_1_tests_1_1_admin_logic_test.html", null ],
    [ "MedicSupplier.Data.Models.Customer", "class_medic_supplier_1_1_data_1_1_models_1_1_customer.html", null ],
    [ "DbContext", null, [
      [ "MedicSupplier.Data.Models.MedicSupplierContext", "class_medic_supplier_1_1_data_1_1_models_1_1_medic_supplier_context.html", null ]
    ] ],
    [ "Exception", null, [
      [ "MedicSupplier.Logic.TooFewInStockException", "class_medic_supplier_1_1_logic_1_1_too_few_in_stock_exception.html", null ]
    ] ],
    [ "MedicSupplier.Logic.IAdminLogic", "interface_medic_supplier_1_1_logic_1_1_i_admin_logic.html", [
      [ "MedicSupplier.Logic.AdminLogic", "class_medic_supplier_1_1_logic_1_1_admin_logic.html", null ]
    ] ],
    [ "IDisposable", null, [
      [ "MedicSupplier.Program.Factory", "class_medic_supplier_1_1_program_1_1_factory.html", null ]
    ] ],
    [ "MedicSupplier.Logic.IOrderLogic", "interface_medic_supplier_1_1_logic_1_1_i_order_logic.html", [
      [ "MedicSupplier.Logic.OrderLogic", "class_medic_supplier_1_1_logic_1_1_order_logic.html", null ]
    ] ],
    [ "MedicSupplier.Repository.IRepository< T >", "interface_medic_supplier_1_1_repository_1_1_i_repository.html", [
      [ "MedicSupplier.Repository.MyRepository< T >", "class_medic_supplier_1_1_repository_1_1_my_repository.html", null ]
    ] ],
    [ "MedicSupplier.Repository.IRepository< Customer >", "interface_medic_supplier_1_1_repository_1_1_i_repository.html", [
      [ "MedicSupplier.Repository.ICustomerRepository", "interface_medic_supplier_1_1_repository_1_1_i_customer_repository.html", [
        [ "MedicSupplier.Repository.CustomerRepository", "class_medic_supplier_1_1_repository_1_1_customer_repository.html", null ]
      ] ]
    ] ],
    [ "MedicSupplier.Repository.IRepository< Manufacturer >", "interface_medic_supplier_1_1_repository_1_1_i_repository.html", [
      [ "MedicSupplier.Repository.IManufacturerRepository", "interface_medic_supplier_1_1_repository_1_1_i_manufacturer_repository.html", [
        [ "MedicSupplier.Repository.ManufacturerRepository", "class_medic_supplier_1_1_repository_1_1_manufacturer_repository.html", null ]
      ] ]
    ] ],
    [ "MedicSupplier.Repository.IRepository< Order >", "interface_medic_supplier_1_1_repository_1_1_i_repository.html", [
      [ "MedicSupplier.Repository.IOrderRepository", "interface_medic_supplier_1_1_repository_1_1_i_order_repository.html", [
        [ "MedicSupplier.Repository.OrderRepository", "class_medic_supplier_1_1_repository_1_1_order_repository.html", null ]
      ] ]
    ] ],
    [ "MedicSupplier.Repository.IRepository< Product >", "interface_medic_supplier_1_1_repository_1_1_i_repository.html", [
      [ "MedicSupplier.Repository.IProductRepository", "interface_medic_supplier_1_1_repository_1_1_i_product_repository.html", [
        [ "MedicSupplier.Repository.ProductRepository", "class_medic_supplier_1_1_repository_1_1_product_repository.html", null ]
      ] ]
    ] ],
    [ "MedicSupplier.Program.MainClass", "class_medic_supplier_1_1_program_1_1_main_class.html", null ],
    [ "MedicSupplier.Data.Models.Manufacturer", "class_medic_supplier_1_1_data_1_1_models_1_1_manufacturer.html", null ],
    [ "MedicSupplier.Program.MenuUI", "class_medic_supplier_1_1_program_1_1_menu_u_i.html", null ],
    [ "MedicSupplier.Repository.MyRepository< Customer >", "class_medic_supplier_1_1_repository_1_1_my_repository.html", [
      [ "MedicSupplier.Repository.CustomerRepository", "class_medic_supplier_1_1_repository_1_1_customer_repository.html", null ]
    ] ],
    [ "MedicSupplier.Repository.MyRepository< Manufacturer >", "class_medic_supplier_1_1_repository_1_1_my_repository.html", [
      [ "MedicSupplier.Repository.ManufacturerRepository", "class_medic_supplier_1_1_repository_1_1_manufacturer_repository.html", null ]
    ] ],
    [ "MedicSupplier.Repository.MyRepository< Order >", "class_medic_supplier_1_1_repository_1_1_my_repository.html", [
      [ "MedicSupplier.Repository.OrderRepository", "class_medic_supplier_1_1_repository_1_1_order_repository.html", null ]
    ] ],
    [ "MedicSupplier.Repository.MyRepository< Product >", "class_medic_supplier_1_1_repository_1_1_my_repository.html", [
      [ "MedicSupplier.Repository.ProductRepository", "class_medic_supplier_1_1_repository_1_1_product_repository.html", null ]
    ] ],
    [ "MedicSupplier.Data.Models.Order", "class_medic_supplier_1_1_data_1_1_models_1_1_order.html", null ],
    [ "MedicSupplier.Tests.OrderLogicTest", "class_medic_supplier_1_1_tests_1_1_order_logic_test.html", null ],
    [ "MedicSupplier.Data.Models.Product", "class_medic_supplier_1_1_data_1_1_models_1_1_product.html", null ],
    [ "MedicSupplier.Logic.ProductbyClassificationResult", "class_medic_supplier_1_1_logic_1_1_productby_classification_result.html", null ]
];