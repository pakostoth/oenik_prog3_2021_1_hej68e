var class_medic_supplier_1_1_repository_1_1_product_repository =
[
    [ "ProductRepository", "class_medic_supplier_1_1_repository_1_1_product_repository.html#a77a164cc4bc1f4eac92232bec6858c74", null ],
    [ "ChangeClassification", "class_medic_supplier_1_1_repository_1_1_product_repository.html#af092efea079dadb5d22abaf107983f7e", null ],
    [ "ChangeInstock", "class_medic_supplier_1_1_repository_1_1_product_repository.html#a2de9e6d6475aafaa8ddc76d522e9f5c6", null ],
    [ "ChangeName", "class_medic_supplier_1_1_repository_1_1_product_repository.html#a3c3507765cbfe75e9fabcd35ca821e9e", null ],
    [ "ChangePrice", "class_medic_supplier_1_1_repository_1_1_product_repository.html#a2761d8c3a4105e982fea89ebe272433a", null ],
    [ "ChangeReorder", "class_medic_supplier_1_1_repository_1_1_product_repository.html#a38e08e5ee4e0e12949a5bc3c36481a8b", null ],
    [ "GetOne", "class_medic_supplier_1_1_repository_1_1_product_repository.html#afbed3312c6cde512585049dc2d3d81ec", null ]
];