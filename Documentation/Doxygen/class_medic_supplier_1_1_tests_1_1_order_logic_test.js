var class_medic_supplier_1_1_tests_1_1_order_logic_test =
[
    [ "Setup", "class_medic_supplier_1_1_tests_1_1_order_logic_test.html#a209c49a6be8cd659a0978f7705825555", null ],
    [ "TestAddOrder", "class_medic_supplier_1_1_tests_1_1_order_logic_test.html#ac14c10ce14a4b329b7d666db5243b6e8", null ],
    [ "TestCustomersWithOrder", "class_medic_supplier_1_1_tests_1_1_order_logic_test.html#adeed4f1ca2cc34b447c7d01d3fd4048f", null ],
    [ "TestGetOneProduct", "class_medic_supplier_1_1_tests_1_1_order_logic_test.html#aa71da393de03ddfd36a73d3ed40f178d", null ],
    [ "TestProductsAndOrdersbyClassification", "class_medic_supplier_1_1_tests_1_1_order_logic_test.html#a87d33d5350f8d53cd0fc4a938458ff58", null ],
    [ "Customers", "class_medic_supplier_1_1_tests_1_1_order_logic_test.html#a4e90c41488cbf5c5f8426741bffbd7ea", null ],
    [ "OLogic", "class_medic_supplier_1_1_tests_1_1_order_logic_test.html#ab9cd96221746dcd60029b047221810ad", null ],
    [ "Orders", "class_medic_supplier_1_1_tests_1_1_order_logic_test.html#a2653158b5a0ee1382b601025e15544b6", null ],
    [ "Products", "class_medic_supplier_1_1_tests_1_1_order_logic_test.html#aec7ace59ef6dd3cf90a2382d0d4af11d", null ]
];