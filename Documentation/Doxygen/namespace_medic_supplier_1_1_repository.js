var namespace_medic_supplier_1_1_repository =
[
    [ "CustomerRepository", "class_medic_supplier_1_1_repository_1_1_customer_repository.html", "class_medic_supplier_1_1_repository_1_1_customer_repository" ],
    [ "ManufacturerRepository", "class_medic_supplier_1_1_repository_1_1_manufacturer_repository.html", "class_medic_supplier_1_1_repository_1_1_manufacturer_repository" ],
    [ "MyRepository", "class_medic_supplier_1_1_repository_1_1_my_repository.html", "class_medic_supplier_1_1_repository_1_1_my_repository" ],
    [ "OrderRepository", "class_medic_supplier_1_1_repository_1_1_order_repository.html", "class_medic_supplier_1_1_repository_1_1_order_repository" ],
    [ "ProductRepository", "class_medic_supplier_1_1_repository_1_1_product_repository.html", "class_medic_supplier_1_1_repository_1_1_product_repository" ],
    [ "ICustomerRepository", "interface_medic_supplier_1_1_repository_1_1_i_customer_repository.html", "interface_medic_supplier_1_1_repository_1_1_i_customer_repository" ],
    [ "IManufacturerRepository", "interface_medic_supplier_1_1_repository_1_1_i_manufacturer_repository.html", "interface_medic_supplier_1_1_repository_1_1_i_manufacturer_repository" ],
    [ "IOrderRepository", "interface_medic_supplier_1_1_repository_1_1_i_order_repository.html", "interface_medic_supplier_1_1_repository_1_1_i_order_repository" ],
    [ "IProductRepository", "interface_medic_supplier_1_1_repository_1_1_i_product_repository.html", "interface_medic_supplier_1_1_repository_1_1_i_product_repository" ],
    [ "IRepository", "interface_medic_supplier_1_1_repository_1_1_i_repository.html", "interface_medic_supplier_1_1_repository_1_1_i_repository" ]
];