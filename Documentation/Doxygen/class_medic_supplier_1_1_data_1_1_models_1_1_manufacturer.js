var class_medic_supplier_1_1_data_1_1_models_1_1_manufacturer =
[
    [ "Manufacturer", "class_medic_supplier_1_1_data_1_1_models_1_1_manufacturer.html#a28e2b92e605bca9bc2af54e695ff50c7", null ],
    [ "Manufacturer", "class_medic_supplier_1_1_data_1_1_models_1_1_manufacturer.html#aa9d4f638dbae0a402d71c72b3ee14ef4", null ],
    [ "Manufacturer", "class_medic_supplier_1_1_data_1_1_models_1_1_manufacturer.html#abedd8323ba502621c7a10a2bf61cefee", null ],
    [ "Equals", "class_medic_supplier_1_1_data_1_1_models_1_1_manufacturer.html#a002f312c5dc26f1d30e504f194fd54e5", null ],
    [ "GetHashCode", "class_medic_supplier_1_1_data_1_1_models_1_1_manufacturer.html#af0e437f429ce23401421000a3899b7d2", null ],
    [ "ToString", "class_medic_supplier_1_1_data_1_1_models_1_1_manufacturer.html#a338d216c8911ec484077d83555a5ccd8", null ],
    [ "Address", "class_medic_supplier_1_1_data_1_1_models_1_1_manufacturer.html#a2e48f5614fefd7d81f7eb736c1e902d7", null ],
    [ "Country", "class_medic_supplier_1_1_data_1_1_models_1_1_manufacturer.html#a0e90fe444ac14eab6e9a45aba2aea7d6", null ],
    [ "Email", "class_medic_supplier_1_1_data_1_1_models_1_1_manufacturer.html#ada6b95c9cc3fc68c24849ad223ce0e0d", null ],
    [ "ManufacturerID", "class_medic_supplier_1_1_data_1_1_models_1_1_manufacturer.html#a18fe86d5cbe6a29e87621dacc967e90e", null ],
    [ "Name", "class_medic_supplier_1_1_data_1_1_models_1_1_manufacturer.html#ad8f0ef74cc145e56cea9248f29b35b80", null ],
    [ "PhoneNumber", "class_medic_supplier_1_1_data_1_1_models_1_1_manufacturer.html#a68a0cbd5b85cb9dc308a6af3d071b8e0", null ],
    [ "Products", "class_medic_supplier_1_1_data_1_1_models_1_1_manufacturer.html#ab60ecf1279266b817fe661a7954c0479", null ],
    [ "Revenue", "class_medic_supplier_1_1_data_1_1_models_1_1_manufacturer.html#a62ca0721faf9335ed410087edc1fa41e", null ]
];