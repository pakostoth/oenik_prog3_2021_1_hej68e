var interface_medic_supplier_1_1_logic_1_1_i_order_logic =
[
    [ "AddOrder", "interface_medic_supplier_1_1_logic_1_1_i_order_logic.html#a02180efb15ba766ba4326c0f7141c3f9", null ],
    [ "CustomersWithOrder", "interface_medic_supplier_1_1_logic_1_1_i_order_logic.html#a355a59fd6c433f42d1ff737e988a800a", null ],
    [ "CustomersWithOrderAsync", "interface_medic_supplier_1_1_logic_1_1_i_order_logic.html#a49e13c20d703c536fc5562ffd5e629aa", null ],
    [ "DeleteOrder", "interface_medic_supplier_1_1_logic_1_1_i_order_logic.html#aab190fa613273e7c7def4a2d9b2bd2d1", null ],
    [ "GetAllOrder", "interface_medic_supplier_1_1_logic_1_1_i_order_logic.html#abf88e1233f34875aceaa46aeee107db0", null ],
    [ "GetAllProduct", "interface_medic_supplier_1_1_logic_1_1_i_order_logic.html#a48279def12e9fb53d438a335fa8e0362", null ],
    [ "GetOneOrder", "interface_medic_supplier_1_1_logic_1_1_i_order_logic.html#a00e2bdffaae5ef192607d774273ca2f6", null ],
    [ "GetOneProduct", "interface_medic_supplier_1_1_logic_1_1_i_order_logic.html#a150df669d7b31be5b0b237ee65285d6f", null ],
    [ "ListAllClassification", "interface_medic_supplier_1_1_logic_1_1_i_order_logic.html#a6d0c69d087b5ae01b738bf039ed7e098", null ],
    [ "ProductsAndOrdersbyClassification", "interface_medic_supplier_1_1_logic_1_1_i_order_logic.html#a91fe99a72112823517a34f907fb48bdb", null ],
    [ "ProductsAndOrdersbyClassificationAsync", "interface_medic_supplier_1_1_logic_1_1_i_order_logic.html#a455ff7edaabc4532bd7ee965a7108659", null ],
    [ "UpdateLastOrderDate", "interface_medic_supplier_1_1_logic_1_1_i_order_logic.html#a33e9f91b2029619d5b77cb4db5849e9c", null ],
    [ "UpdateOrderDeadline", "interface_medic_supplier_1_1_logic_1_1_i_order_logic.html#a40db0bd195ae8242510d0f6c4b64bbcc", null ],
    [ "UpdateOrderQuantity", "interface_medic_supplier_1_1_logic_1_1_i_order_logic.html#aeb8946e7dae886a6a0d08623f5250af1", null ],
    [ "UpdateProductClassification", "interface_medic_supplier_1_1_logic_1_1_i_order_logic.html#a37d51ceb2013e12f32f7fb714ac356bb", null ],
    [ "UpdateProductOrderLevel", "interface_medic_supplier_1_1_logic_1_1_i_order_logic.html#af6066936fba93c785b938a11b21d4495", null ],
    [ "UpdateProductPrice", "interface_medic_supplier_1_1_logic_1_1_i_order_logic.html#a036cf89dad1a1bc6cd6be00f1adb159c", null ],
    [ "UpdateProductQuantity", "interface_medic_supplier_1_1_logic_1_1_i_order_logic.html#a1508cc12bb1315cf1c15395576fcb97e", null ]
];