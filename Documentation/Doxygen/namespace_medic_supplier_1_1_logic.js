var namespace_medic_supplier_1_1_logic =
[
    [ "AdminLogic", "class_medic_supplier_1_1_logic_1_1_admin_logic.html", "class_medic_supplier_1_1_logic_1_1_admin_logic" ],
    [ "TooFewInStockException", "class_medic_supplier_1_1_logic_1_1_too_few_in_stock_exception.html", "class_medic_supplier_1_1_logic_1_1_too_few_in_stock_exception" ],
    [ "IAdminLogic", "interface_medic_supplier_1_1_logic_1_1_i_admin_logic.html", "interface_medic_supplier_1_1_logic_1_1_i_admin_logic" ],
    [ "IOrderLogic", "interface_medic_supplier_1_1_logic_1_1_i_order_logic.html", "interface_medic_supplier_1_1_logic_1_1_i_order_logic" ],
    [ "OrderLogic", "class_medic_supplier_1_1_logic_1_1_order_logic.html", "class_medic_supplier_1_1_logic_1_1_order_logic" ],
    [ "ProductbyClassificationResult", "class_medic_supplier_1_1_logic_1_1_productby_classification_result.html", "class_medic_supplier_1_1_logic_1_1_productby_classification_result" ]
];