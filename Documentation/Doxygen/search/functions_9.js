var searchData=
[
  ['onconfiguring_303',['OnConfiguring',['../class_medic_supplier_1_1_data_1_1_models_1_1_medic_supplier_context.html#a44645e75116b86e7ac800cfe7542ecb0',1,'MedicSupplier::Data::Models::MedicSupplierContext']]],
  ['onmodelcreating_304',['OnModelCreating',['../class_medic_supplier_1_1_data_1_1_models_1_1_medic_supplier_context.html#a63dd96e64f9b59e1fc2491c34812462c',1,'MedicSupplier::Data::Models::MedicSupplierContext']]],
  ['order_305',['Order',['../class_medic_supplier_1_1_data_1_1_models_1_1_order.html#a494c990d9773c29097dc857ae6850b90',1,'MedicSupplier.Data.Models.Order.Order(int orderID, int productID, int customerID, int quantity, DateTime deadline)'],['../class_medic_supplier_1_1_data_1_1_models_1_1_order.html#ac900a5f5b1477f1183ae86f7e8000356',1,'MedicSupplier.Data.Models.Order.Order(int productID, int customerID, int quantity, DateTime deadline)'],['../class_medic_supplier_1_1_data_1_1_models_1_1_order.html#a5cc72035e0a28c48fab9c5f0c1520de9',1,'MedicSupplier.Data.Models.Order.Order()']]],
  ['orderlogic_306',['OrderLogic',['../class_medic_supplier_1_1_logic_1_1_order_logic.html#a9ceb648bbeff289babd7ffef455ce281',1,'MedicSupplier::Logic::OrderLogic']]],
  ['orderrepository_307',['OrderRepository',['../class_medic_supplier_1_1_repository_1_1_order_repository.html#ac3bc1e0e3c0d28f12a648612d0d5be62',1,'MedicSupplier::Repository::OrderRepository']]]
];
