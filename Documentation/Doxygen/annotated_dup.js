var annotated_dup =
[
    [ "MedicSupplier", "namespace_medic_supplier.html", [
      [ "Data", "namespace_medic_supplier_1_1_data.html", [
        [ "Models", "namespace_medic_supplier_1_1_data_1_1_models.html", [
          [ "Customer", "class_medic_supplier_1_1_data_1_1_models_1_1_customer.html", "class_medic_supplier_1_1_data_1_1_models_1_1_customer" ],
          [ "Manufacturer", "class_medic_supplier_1_1_data_1_1_models_1_1_manufacturer.html", "class_medic_supplier_1_1_data_1_1_models_1_1_manufacturer" ],
          [ "MedicSupplierContext", "class_medic_supplier_1_1_data_1_1_models_1_1_medic_supplier_context.html", "class_medic_supplier_1_1_data_1_1_models_1_1_medic_supplier_context" ],
          [ "Order", "class_medic_supplier_1_1_data_1_1_models_1_1_order.html", "class_medic_supplier_1_1_data_1_1_models_1_1_order" ],
          [ "Product", "class_medic_supplier_1_1_data_1_1_models_1_1_product.html", "class_medic_supplier_1_1_data_1_1_models_1_1_product" ]
        ] ]
      ] ],
      [ "Logic", "namespace_medic_supplier_1_1_logic.html", [
        [ "AdminLogic", "class_medic_supplier_1_1_logic_1_1_admin_logic.html", "class_medic_supplier_1_1_logic_1_1_admin_logic" ],
        [ "TooFewInStockException", "class_medic_supplier_1_1_logic_1_1_too_few_in_stock_exception.html", "class_medic_supplier_1_1_logic_1_1_too_few_in_stock_exception" ],
        [ "IAdminLogic", "interface_medic_supplier_1_1_logic_1_1_i_admin_logic.html", "interface_medic_supplier_1_1_logic_1_1_i_admin_logic" ],
        [ "IOrderLogic", "interface_medic_supplier_1_1_logic_1_1_i_order_logic.html", "interface_medic_supplier_1_1_logic_1_1_i_order_logic" ],
        [ "OrderLogic", "class_medic_supplier_1_1_logic_1_1_order_logic.html", "class_medic_supplier_1_1_logic_1_1_order_logic" ],
        [ "ProductbyClassificationResult", "class_medic_supplier_1_1_logic_1_1_productby_classification_result.html", "class_medic_supplier_1_1_logic_1_1_productby_classification_result" ]
      ] ],
      [ "Program", "namespace_medic_supplier_1_1_program.html", [
        [ "Factory", "class_medic_supplier_1_1_program_1_1_factory.html", "class_medic_supplier_1_1_program_1_1_factory" ],
        [ "MainClass", "class_medic_supplier_1_1_program_1_1_main_class.html", "class_medic_supplier_1_1_program_1_1_main_class" ],
        [ "MenuUI", "class_medic_supplier_1_1_program_1_1_menu_u_i.html", "class_medic_supplier_1_1_program_1_1_menu_u_i" ]
      ] ],
      [ "Repository", "namespace_medic_supplier_1_1_repository.html", [
        [ "CustomerRepository", "class_medic_supplier_1_1_repository_1_1_customer_repository.html", "class_medic_supplier_1_1_repository_1_1_customer_repository" ],
        [ "ManufacturerRepository", "class_medic_supplier_1_1_repository_1_1_manufacturer_repository.html", "class_medic_supplier_1_1_repository_1_1_manufacturer_repository" ],
        [ "MyRepository", "class_medic_supplier_1_1_repository_1_1_my_repository.html", "class_medic_supplier_1_1_repository_1_1_my_repository" ],
        [ "OrderRepository", "class_medic_supplier_1_1_repository_1_1_order_repository.html", "class_medic_supplier_1_1_repository_1_1_order_repository" ],
        [ "ProductRepository", "class_medic_supplier_1_1_repository_1_1_product_repository.html", "class_medic_supplier_1_1_repository_1_1_product_repository" ],
        [ "ICustomerRepository", "interface_medic_supplier_1_1_repository_1_1_i_customer_repository.html", "interface_medic_supplier_1_1_repository_1_1_i_customer_repository" ],
        [ "IManufacturerRepository", "interface_medic_supplier_1_1_repository_1_1_i_manufacturer_repository.html", "interface_medic_supplier_1_1_repository_1_1_i_manufacturer_repository" ],
        [ "IOrderRepository", "interface_medic_supplier_1_1_repository_1_1_i_order_repository.html", "interface_medic_supplier_1_1_repository_1_1_i_order_repository" ],
        [ "IProductRepository", "interface_medic_supplier_1_1_repository_1_1_i_product_repository.html", "interface_medic_supplier_1_1_repository_1_1_i_product_repository" ],
        [ "IRepository", "interface_medic_supplier_1_1_repository_1_1_i_repository.html", "interface_medic_supplier_1_1_repository_1_1_i_repository" ]
      ] ],
      [ "Tests", "namespace_medic_supplier_1_1_tests.html", [
        [ "AdminLogicTest", "class_medic_supplier_1_1_tests_1_1_admin_logic_test.html", "class_medic_supplier_1_1_tests_1_1_admin_logic_test" ],
        [ "OrderLogicTest", "class_medic_supplier_1_1_tests_1_1_order_logic_test.html", "class_medic_supplier_1_1_tests_1_1_order_logic_test" ]
      ] ]
    ] ]
];