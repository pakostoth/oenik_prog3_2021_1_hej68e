var class_medic_supplier_1_1_data_1_1_models_1_1_customer =
[
    [ "Customer", "class_medic_supplier_1_1_data_1_1_models_1_1_customer.html#adf04c8c00645ea955eef55d047f66775", null ],
    [ "Customer", "class_medic_supplier_1_1_data_1_1_models_1_1_customer.html#aa5de0cfb64e90eb3da98f506b0ca95d0", null ],
    [ "Customer", "class_medic_supplier_1_1_data_1_1_models_1_1_customer.html#add07fd52c4cde8d35109f1f6b22dacdb", null ],
    [ "Equals", "class_medic_supplier_1_1_data_1_1_models_1_1_customer.html#af52f70a3925fe55311496fc0dc37843f", null ],
    [ "GetHashCode", "class_medic_supplier_1_1_data_1_1_models_1_1_customer.html#a4eaa2c808b0bb99c114dbf8a2562a914", null ],
    [ "ToString", "class_medic_supplier_1_1_data_1_1_models_1_1_customer.html#acdef338855906f689c54a9d70a004d41", null ],
    [ "Address", "class_medic_supplier_1_1_data_1_1_models_1_1_customer.html#a9db69e5292c788ab7b0a1fe9eb1ced22", null ],
    [ "Country", "class_medic_supplier_1_1_data_1_1_models_1_1_customer.html#a0611303a1dbbabc17d72ba9ed51d062d", null ],
    [ "CustomerID", "class_medic_supplier_1_1_data_1_1_models_1_1_customer.html#ad4a336eba3b2ce509364bad65e2ea335", null ],
    [ "Email", "class_medic_supplier_1_1_data_1_1_models_1_1_customer.html#a6d95d6cfe1a577f0635a0e8b6a574955", null ],
    [ "LastOrderDate", "class_medic_supplier_1_1_data_1_1_models_1_1_customer.html#a48b304e27a6ccc9f560be060ca22b1eb", null ],
    [ "Name", "class_medic_supplier_1_1_data_1_1_models_1_1_customer.html#aad7e3fb54ec050efcae6d318b800bc20", null ],
    [ "Orders", "class_medic_supplier_1_1_data_1_1_models_1_1_customer.html#a1853770aff1984b954ba8374ce223d01", null ],
    [ "PhoneNumber", "class_medic_supplier_1_1_data_1_1_models_1_1_customer.html#abbbe4a918ef8aa56d8cfdb2fa7765727", null ]
];