var class_medic_supplier_1_1_data_1_1_models_1_1_product =
[
    [ "Product", "class_medic_supplier_1_1_data_1_1_models_1_1_product.html#ab302e92d60cb073d6b772441e35454ec", null ],
    [ "Product", "class_medic_supplier_1_1_data_1_1_models_1_1_product.html#a823214c1c50a97dc7dfba9eca5c9e576", null ],
    [ "Product", "class_medic_supplier_1_1_data_1_1_models_1_1_product.html#a5100df5d9e33fa1b28b20e9a7c3e0ae9", null ],
    [ "Equals", "class_medic_supplier_1_1_data_1_1_models_1_1_product.html#a6f088f0f531e74f292e09fbcfa48881c", null ],
    [ "GetHashCode", "class_medic_supplier_1_1_data_1_1_models_1_1_product.html#a568eaf353e629e70968f895b033195a4", null ],
    [ "ToString", "class_medic_supplier_1_1_data_1_1_models_1_1_product.html#a6d7e80924c24df596ce11ff7e60b1d80", null ],
    [ "Classification", "class_medic_supplier_1_1_data_1_1_models_1_1_product.html#af354a89b5847a8e5d6869d41a2ad81b1", null ],
    [ "InStock", "class_medic_supplier_1_1_data_1_1_models_1_1_product.html#a08d09cabb039bec658b6d02b1967275a", null ],
    [ "Manufacturer", "class_medic_supplier_1_1_data_1_1_models_1_1_product.html#ad2c361d65883272493d028c44668354d", null ],
    [ "ManufacturerID", "class_medic_supplier_1_1_data_1_1_models_1_1_product.html#adfc1158c00f38a1e46711f63c3bb780f", null ],
    [ "Name", "class_medic_supplier_1_1_data_1_1_models_1_1_product.html#a781d0069c9312a4ee30b9820d85d8974", null ],
    [ "Orders", "class_medic_supplier_1_1_data_1_1_models_1_1_product.html#a7faaab48b31fbc663e6157d7b8cf53c0", null ],
    [ "Price", "class_medic_supplier_1_1_data_1_1_models_1_1_product.html#ad9c065e812685fe445a1c0159041bc6f", null ],
    [ "ProductID", "class_medic_supplier_1_1_data_1_1_models_1_1_product.html#aea646c1a32edf3df61481e0cf65d6439", null ],
    [ "ReorderLevel", "class_medic_supplier_1_1_data_1_1_models_1_1_product.html#a21f3b5304672398152c6102c306b4e08", null ]
];